// Axes.js :: a class for drawing the axes of a chart
// (c) 2020 Jose Alvarez

class Axes {

    constructor(world) {
         
        this.x=0.0;
        this.y=0.0;
        this.z=0.0;

        this.xScale=1.0;
        this.yScale=1.0;

        this.maxX=0.0;
        this.minX=0.0;
        
        this.maxY=0.0;
        this.minY=0.0;
        this.yRange=0.0;

        this.samples=0;
        this.deltaX=1.0;

        this.r=0.6;
        this.g=0.6;
        this.b=0.6;
        this.a=1.0;

        this.vertices=null;
        this.numOfVertices=0;

        this.transMatrix=new Matrix4();
        this.transMatrix.setIdentity();

        this.glObject=new PandoObject(world,null);

    }

    setNumberOfSamples(samples) {
        this.samples=samples;
        this.deltaX=1.0/samples;

    }

    setYRange(minY,maxY) {
        this.minY=minY;
        this.maxY=maxY;
        this.yRange=maxY-minY;
    }

    setPosition(x,y,z) {
        this.x=x;
        this.y=y;
        this.z=0.01;
        //this.transMatrix.setTranslate(x,y,z);
    }

    position(x,y,z) {
        this.x+=x;
        this.y+=y;
        this.z=0.01;

        //this.transMatrix.translate(x,y,z);
    }

    setScale(x,y) {
        this.xScale=x;
        this.yScale=y;
        //this.transMatrix.scale(x,y,1.0);
    }

    scale(x,y) {
        this.xScale*=x;
        this.yScale*=y;
        this.transMatrix.scale(x,y,1.0);
    }

    setColor(r,g,b,a) {
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }

    absYOffset() {
        return this.minY/this.yRange*this.yScale;
    }
    calculateVertices() {
        /*
            X Axis: 2 for the main line + 2*samples/14 (one marker every fortnight)
            Y axis: 2 for the main line + 2*4 auxiliary markers
        */
        this.numOfVertices=(12+this.samples/7)

        this.vertices=new Float32Array(this.numOfVertices*2);

        var yRange=(this.maxY-this.minY);
        var curVertex=0;
        // X axis
        if (this.minY<0) {
            this.vertices[curVertex*2]=0.0;
            //this.vertices[curVertex*2+1]=-this.minY/yRange;
            curVertex++;
            this.vertices[curVertex*2]=1.0;
            //this.vertices[curVertex*2+1]=-this.minY/yRange;
        } else {
            this.vertices[curVertex*2]=0.0;
            this.vertices[curVertex*2+1]=0;
            curVertex++;
            this.vertices[curVertex*2]=1.0;
            this.vertices[curVertex*2+1]=0;
        }
        // Y axis
        curVertex++;
        this.vertices[curVertex*2]=0.0;
        this.vertices[curVertex*2+1]=this.minY/yRange;
        curVertex++;
        this.vertices[curVertex*2]=0.0;
        this.vertices[curVertex*2+1]=this.maxY/yRange;
        curVertex++;

        // Y Ticks
        var deltaY=1.0/5.0; // just to be clear
        var yPos=this.vertices[1]+deltaY;
        while (yPos<this.maxY/this.yRange) {
            this.vertices[curVertex*2]=0.0;
            this.vertices[curVertex*2+1]=yPos;
            curVertex++;
            this.vertices[curVertex*2]=0.03;
            this.vertices[curVertex*2+1]=yPos;
            curVertex++;
            yPos+=deltaY;
        } 
        yPos=-deltaY;
        while (yPos>this.minY/this.yRange) {
            this.vertices[curVertex*2]=0.0;
            this.vertices[curVertex*2+1]=yPos;
            curVertex++;
            this.vertices[curVertex*2]=0.03;
            this.vertices[curVertex*2+1]=yPos;
            curVertex++;
            yPos-=deltaY;
        } 

        // X Ticks
        yPos=this.vertices[1];
        var deltaX=14/this.samples;
        var xPos=deltaX;
        while (curVertex<this.numOfVertices) {
            this.vertices[curVertex*2]=xPos;
            this.vertices[curVertex*2+1]=yPos;
            curVertex++;
            this.vertices[curVertex*2]=xPos;
            this.vertices[curVertex*2+1]=yPos+0.03;
            curVertex++;
            xPos+=deltaX;
        }
    }

    create() {
        this.glObject.use();
        this.glObject.setNumberOfVertices(this.numOfVertices);

        this.glObject.getAttributeLocation('a_Position');
        this.glObject.getUniformLocation('u_Transform');

        this.glObject.initArrayBufferFloat('a_Position',this.vertices,2);
        this.glObject.world.unbindArrayBuffer();
    }

    draw(transMatrix=this.transMatrix) {
        this.glObject.use();
        this.glObject.transMatrix.setIdentity();

        // Calculate the model view project matrix and pass it to u_Transform
        this.glObject.transMatrix.multiply(transMatrix);
        this.glObject.transMatrix.translate(this.x,this.y,0.0);
        this.glObject.transMatrix.scale(this.xScale,this.yScale,1.0);
        this.glObject.setUniform4F('u_Color',this.r,this.g,this.b,this.a);

        this.glObject.drawLines();
        this.glObject.world.unbindArrayBuffer();
    }
};
