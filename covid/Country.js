// Country.js :: a class to hold country information
// (c) 2020 Jose Alvarez

class Country {
    constructor(id,lat,lon,name,world,hud) {
        this.id=id;
        this.name=name;

        this.lat=lat;
        this.lon=lon;

        this.data=null;
        this.dimensions=null;;
        
        this.x=0;
        this.y=0;
        this.z=0;

        this.r=0;
        this.g=0;
        this.b=0;
        this.a=255;

        this.world=world;
        this.hud=hud;
        this.hudContext=hud.getContext('2d');

        this.sparks=null;
        this.axes=null;

        this.commonDeltaAxes=null;

        this.absoluteYRange=0.0;
        this.deltaYRange=0.0;
        this.deltaYMax=0.0;
        this.deltaYMin=0.0;
        
        this.ready=false;

        this.calcXYZ();

        this.initSparks();
        this.initAxes();
    }

    initSparks() {
        this.sparks={
            infected: new Spark(this.world),
            active: new Spark(this.world),
            recovered: new Spark(this.world),
            dead: new Spark(this.world),

            deltaInfected: new Spark(this.world),
            deltaActive: new Spark(this.world),
            deltaRecovered: new Spark(this.world),
            deltaDead: new Spark(this.world)
        }
    }

    initAxes() {
        this.axes={
            infected: new Axes(this.world),
            active: new Axes(this.world),
            recovered: new Axes(this.world),
            dead: new Axes(this.world),

            deltaInfected: new Axes(this.world),
            deltaActive: new Axes(this.world),
            deltaRecovered: new Axes(this.world),
            deltaDead: new Axes(this.world)
        }
        this.commonDeltaAxes=new Axes(this.world);
    }

    isReady() {
        if (this.ready && this.sparks['infected'].isReady()) return true;
    }

    setVertexShader(vertex) {
        for (var key in this.sparks) {
            this.sparks[key].glObject.setVertexShader(vertex);
            this.axes[key].glObject.setVertexShader(vertex);
        }
        this.commonDeltaAxes.glObject.setVertexShader(vertex);
    }

    setFragmentShader(fragment) {
        for (var key in this.sparks) {
            this.sparks[key].glObject.setFragmentShader(fragment);
            this.axes[key].glObject.setFragmentShader(fragment);
        }  
        this.commonDeltaAxes.glObject.setFragmentShader(fragment);    
    }

    setColor(r,g,b,a=255) {
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }

    calcXYZ() {
        var lat=(90.0-this.lat);
        var Z=lat*Math.PI/180;
        var sinZ=Math.sin(Z);
        var cosZ=Math.cos(Z);

        var lon=(this.lon-90);
        var P=lon*Math.PI/180;
        var sinP=Math.sin(P);
        var cosP=Math.cos(P);

        this.x=sinZ*sinP;
        this.y=cosZ;
        this.z=sinZ*cosP;
    }

    setSamples(samples) {
        for (var key in this.sparks) {
            this.sparks[key].setNumberOfSamples(samples);
            this.axes[key].setNumberOfSamples(samples);
        }
        this.commonDeltaAxes.setNumberOfSamples(samples);
    }

    normalizeSparks() {
        for (var key in this.sparks) {
            this.sparks[key].normalizeSamples();
            if (key.includes('delta')) {
                if (this.sparks[key].maxY>this.deltaYMax) this.deltaYMax=this.sparks[key].maxY;
                if (this.sparks[key].minY<this.deltaYMin) this.deltaYMin=this.sparks[key].minY;
            } else {
                if (this.sparks[key].yRange>this.absoluteYRange) this.absoluteYRange=this.sparks[key].yRange;
            }
        }
        this.deltaYRange=this.deltaYMax-this.deltaYMin;
        /*
        console.log("deltaYRange: "+this.deltaYRange);
        console.log("deltaYMax: "+this.deltaYMax);
        console.log("deltaYMin: "+this.deltaYMin);
        */
        this.commonDeltaAxes.setYRange(this.deltaYMin,this.deltaYMax);
        this.commonDeltaAxes.calculateVertices();
    }

    arrangeGroupSparks(xPos,yPos,xScale,yScale) {

        var xDistance=1.1;
        var yDistance=1.3;

        var yPosCurrent=yPos;
        this.sparks['infected'].setScale(xScale,yScale*this.sparks['infected'].yRange/this.absoluteYRange);
        this.sparks['infected'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['infected'].setColor(0.8,0.8,0.0,1.0);

        //yPosCurrent-=yScale*yDistance;
        this.sparks['active'].setScale(xScale,yScale*this.sparks['active'].yRange/this.absoluteYRange);
        this.sparks['active'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['active'].setColor(0.8,0.0,0.0,1.0);

        //yPosCurrent-=yScale*yDistance;
        this.sparks['recovered'].setScale(xScale,yScale*this.sparks['recovered'].yRange/this.absoluteYRange);
        this.sparks['recovered'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['recovered'].setColor(0.0,0.8,0.0,1.0);

        //yPosCurrent-=yScale*yDistance;
        this.sparks['dead'].setScale(xScale,yScale*this.sparks['dead'].yRange/this.absoluteYRange);
        this.sparks['dead'].setPosition(xPos,yPosCurrent,0.0);
        this.axes['dead'].setScale(xScale,yScale);
        this.axes['dead'].setPosition(xPos,yPosCurrent,0.0);
        
        yPos-=this.deltaYMin/this.deltaYRange;
        yPosCurrent=yPos-yScale*yDistance;//-this.deltaYMin+this.sparks['deltaInfected'].yMin;
        this.sparks['deltaInfected'].setScale(xScale,yScale*this.sparks['deltaInfected'].yRange/this.deltaYRange);
        this.sparks['deltaInfected'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['deltaInfected'].setColor(0.8,0.8,0.0,1.0);

        yPosCurrent=yPos-yScale*yDistance;//+this.deltaYMin-this.sparks['deltaActive'].yMin;
        this.sparks['deltaActive'].setScale(xScale,yScale*this.sparks['deltaActive'].yRange/this.deltaYRange);
        this.sparks['deltaActive'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['deltaActive'].setColor(0.8,0.0,0.0,1.0);
        
        yPosCurrent=yPos-yScale*yDistance;//+this.deltaYMin-this.sparks['deltaRecovered'].yMin;
        this.sparks['deltaRecovered'].setScale(xScale,yScale*this.sparks['deltaRecovered'].yRange/this.deltaYRange);
        this.sparks['deltaRecovered'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['deltaRecovered'].setColor(0.0,0.8,0.0,1.0);
        
        yPosCurrent=yPos-yScale*yDistance;//+this.deltaYMin-this.sparks['deltaDead'].yMin;
        this.sparks['deltaDead'].setScale(xScale,yScale*this.sparks['deltaDead'].yRange/this.deltaYRange);
        this.sparks['deltaDead'].setPosition(xPos,yPosCurrent,0.0);
        
        this.commonDeltaAxes.setScale(xScale,yScale*this.commonDeltaAxes.yRange/this.deltaYRange);
        this.commonDeltaAxes.setPosition(xPos,yPosCurrent,0.0);
    }

    arrangeIndividualSparks(xPos,yPos,xScale,yScale) {

        var xDistance=1.1;
        var yDistance=1.5;

        var yPosCurrent=yPos;
        this.sparks['infected'].setScale(xScale,yScale);
        this.sparks['infected'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['infected'].setColor(0.8,0.8,0.0,1.0);
        this.axes['infected'].setScale(xScale,yScale);
        this.axes['infected'].setPosition(xPos,yPosCurrent,0.0);
        this.axes['infected'].setColor(0.8,0.8,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['active'].setScale(xScale,yScale);
        this.sparks['active'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['active'].setColor(0.8,0.0,0.0,1.0);
        this.axes['active'].setScale(xScale,yScale);
        this.axes['active'].setPosition(xPos,yPosCurrent,0.0);
        this.axes['active'].setColor(0.8,0.0,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['recovered'].setScale(xScale,yScale);
        this.sparks['recovered'].setPosition(xPos,yPosCurrent,0.0);
        this.sparks['recovered'].setColor(0.0,0.8,0.0,1.0);
        this.axes['recovered'].setScale(xScale,yScale);
        this.axes['recovered'].setPosition(xPos,yPosCurrent,0.0);
        this.axes['recovered'].setColor(0.0,0.8,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['dead'].setScale(xScale,yScale);
        this.sparks['dead'].setPosition(xPos,yPosCurrent,0.0);
        this.axes['dead'].setScale(xScale,yScale);
        this.axes['dead'].setPosition(xPos,yPosCurrent,0.0);

        yPosCurrent=yPos;
        xPos-=xPos*xDistance;
        this.sparks['deltaInfected'].setScale(xScale,yScale);
        this.sparks['deltaInfected'].setPosition(xPos,yPosCurrent-this.sparks['deltaInfected'].absYOffset(),0.0);
        this.sparks['deltaInfected'].setColor(0.8,0.8,0.0,1.0);
        this.axes['deltaInfected'].setScale(xScale,yScale);
        this.axes['deltaInfected'].setPosition(xPos,yPosCurrent-this.axes['deltaInfected'].absYOffset(),0.0);
        this.axes['deltaInfected'].setColor(0.8,0.8,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['deltaActive'].setScale(xScale,yScale);
        this.sparks['deltaActive'].setPosition(xPos,yPosCurrent-this.sparks['deltaActive'].absYOffset(),0.0);
        this.sparks['deltaActive'].setColor(0.8,0.0,0.0,1.0);
        this.axes['deltaActive'].setScale(xScale,yScale);
        this.axes['deltaActive'].setPosition(xPos,yPosCurrent-this.axes['deltaActive'].absYOffset(),0.0);
        this.axes['deltaActive'].setColor(0.8,0.0,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['deltaRecovered'].setScale(xScale,yScale);
        this.sparks['deltaRecovered'].setPosition(xPos,yPosCurrent-this.sparks['deltaRecovered'].absYOffset(),0.0);
        this.sparks['deltaRecovered'].setColor(0.0,0.8,0.0,1.0);
        this.axes['deltaRecovered'].setScale(xScale,yScale);
        this.axes['deltaRecovered'].setPosition(xPos,yPosCurrent-this.axes['deltaRecovered'].absYOffset(),0.0);
        this.axes['deltaRecovered'].setColor(0.0,0.8,0.0,1.0);
        yPosCurrent-=yScale*yDistance;
        this.sparks['deltaDead'].setScale(xScale,yScale);
        this.sparks['deltaDead'].setPosition(xPos,yPosCurrent-this.sparks['deltaDead'].absYOffset(),0.0);
        this.axes['deltaDead'].setScale(xScale,yScale);
        this.axes['deltaDead'].setPosition(xPos,yPosCurrent-this.axes['deltaDead'].absYOffset(),0.0);
    }

    setData(csv) {
        var dimensions={rows:0,columns:0};
        this.data=PandoGL.csv2StringArray(csv,dimensions);
        this.dimensions=dimensions;

        this.setSamples(dimensions.rows-1);
        for (var i=1;i<dimensions.rows;i++) {
            this.sparks['infected'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+1]));
            this.sparks['active'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+2]));
            this.sparks['recovered'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+3]));
            this.sparks['dead'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+4]));

            this.sparks['deltaInfected'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+5]));
            this.sparks['deltaActive'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+6]));
            this.sparks['deltaRecovered'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+7]));
            this.sparks['deltaDead'].setSample(i-1,parseFloat(this.data[i*dimensions.columns+8]));
            //console.log("Sample: "+data[i*dimensions.columns+1]);
        }
        for (var key in this.sparks) {
            this.axes[key].setYRange(this.sparks[key].minY,this.sparks[key].maxY);
            this.axes[key].calculateVertices();
        }
        this.normalizeSparks();
        

        this.ready=true;
        //console.log("Country samples: "+dimensions.rows);
    }

    create() {
        for (var key in this.sparks) {
            this.sparks[key].create();
            this.axes[key].create();
        }
        this.commonDeltaAxes.create();
    }

    drawGroup() {
        this.arrangeGroupSparks(-0.9,0.080,1.8,0.75);
        for (var key in this.sparks) {
            this.sparks[key].draw(this.world.viewMatrix);
        }
        this.axes['dead'].draw(this.world.viewMatrix);
        this.commonDeltaAxes.draw(this.world.viewMatrix);
        this.drawGroupHud();
    }

    drawIndividual() {
        this.arrangeIndividualSparks(-0.9,0.400,0.8,0.30);
        for (var key in this.sparks) {
            this.sparks[key].draw(this.world.viewMatrix);
            this.axes[key].draw(this.world.viewMatrix);
        }
        this.drawIndividualHud();
    }

    drawIndividualHud() {

        var lastRow=this.dimensions.columns*(this.dimensions.rows-1);

        this.hudContext.clearRect(0, 0, 600, 600);
        this.hud.width+=0;
        // title
        var xTitle=40;
        var yTitle=40;

        this.hudContext.fillStyle = 'rgba(255, 255, 255, 1)';
        this.hudContext.font='18px Arial,cursive, sans-serif';
        this.hudContext.fillText(this.name+", Updated: "+this.data[lastRow], xTitle, yTitle);

        // type
        this.hudContext.font='16px Arial,cursive, sans-serif';
        var xType=this.hud.width/4.2;
        var yType=this.hud.height/8;
        this.hudContext.fillText('Absolute', xType, yType);
        this.hudContext.fillText('Daily', xType+this.hud.width/2, yType);

        // values

        var labelX=50;
        var deltaLabel=this.hud.height/4.4;
        var labelY=deltaLabel*0.85;

        this.hudContext.fillStyle = 'yellow';
        this.hudContext.fillText(this.data[lastRow+1], labelX, labelY);
        this.hudContext.fillText(this.data[lastRow+5], labelX+this.hud.width/2, labelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'red';
        this.hudContext.fillText(this.data[lastRow+2], labelX, labelY);
        this.hudContext.fillText(this.data[lastRow+6], labelX+this.hud.width/2, labelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'green';
        this.hudContext.fillText(this.data[lastRow+3], labelX, labelY);
        this.hudContext.fillText(this.data[lastRow+7], labelX+this.hud.width/2, labelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'grey';
        this.hudContext.fillText(this.data[lastRow+4], labelX, labelY);
        this.hudContext.fillText(this.data[lastRow+8], labelX+this.hud.width/2, labelY);

        this.hudContext.textAlign='end';
        this.hudContext.font='11px Arial,cursive, sans-serif';
        this.hudContext.fillText('(*) x-axis ticks every two weeks.', this.hud.width-10, this.hud.height-5);

        // Rotate for labels
        this.hudContext.rotate(-Math.PI/2);

        var labelY=25;
        var labelX=-this.hud.width*0.970;
        var deltaLabel=this.hud.width/4.4;

        this.hudContext.textAlign='start';
        this.hudContext.font='15px Arial,cursive, sans-serif';
        this.hudContext.fillStyle = 'grey';
        this.hudContext.fillText('Deaths', labelX, labelY);
        
        labelX+=deltaLabel;
        this.hudContext.fillStyle = 'green';
        this.hudContext.fillText('Recovered', labelX, labelY);

        labelX+=deltaLabel;
        this.hudContext.fillStyle = 'red';
        this.hudContext.fillText('Active', labelX, labelY);
        
        labelX+=deltaLabel;
        this.hudContext.fillStyle = 'yellow';
        this.hudContext.fillText('Infected', labelX, labelY);
        
        
    }

    drawGroupHud() {

        var lastRow=this.dimensions.columns*(this.dimensions.rows-1);

        this.hudContext.clearRect(0, 0, 600, 600);
        this.hud.width+=0;

        // title
        var xTitle=40;
        var yTitle=40;

        this.hudContext.fillStyle = 'rgba(255, 255, 255, 1)';
        this.hudContext.font='18px Arial,cursive, sans-serif';
        this.hudContext.fillText(this.name+", Updated: "+this.data[lastRow], xTitle, yTitle);

        // values

        var labelX=190;
        var deltaLabel=16;
        var deltaLabelY=this.hud.width/2.1;
        var labelY=80;

        this.hudContext.font='14px Arial,cursive, sans-serif';
        this.hudContext.textAlign='end';
        this.hudContext.fillStyle = 'yellow';
        this.hudContext.fillText('Infected: '+this.data[lastRow+1], labelX, labelY);
        this.hudContext.fillText('Infected: '+this.data[lastRow+5], labelX, labelY+deltaLabelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'red';
        this.hudContext.fillText('Active: '+this.data[lastRow+2], labelX, labelY);
        this.hudContext.fillText('Active: '+this.data[lastRow+6], labelX, labelY+deltaLabelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'green';
        this.hudContext.fillText('Recovered: '+this.data[lastRow+3], labelX, labelY);
        this.hudContext.fillText('Recovered: '+this.data[lastRow+7], labelX, labelY+deltaLabelY);

        labelY+=deltaLabel;
        this.hudContext.fillStyle = 'grey';
        this.hudContext.fillText('Deaths: '+this.data[lastRow+4], labelX, labelY);
        this.hudContext.fillText('Deaths: '+this.data[lastRow+8], labelX, labelY+deltaLabelY);

        this.hudContext.font='11px Arial,cursive, sans-serif';
        this.hudContext.fillText('(*) x-axis ticks every two weeks.', this.hud.width-10, this.hud.height-5);

        // Rotate for types
        this.hudContext.rotate(-Math.PI/2);
        var yType=25;
        var xType=-this.hud.width*0.80;

        this.hudContext.textAlign='start';
        this.hudContext.font='16px Arial,cursive, sans-serif';
        this.hudContext.fillText('Daily', xType, yType);
        this.hudContext.fillText('Absolute', xType+this.hud.width/2.5, yType);



    }

    draw(grouping=false) {
        if (grouping) this.drawGroup();
        else this.drawIndividual();
    }
};
