// covid.js :: a 3d covid data console 
// (c) 2020 Jose Alvarez

const DATA_DIR='../data/'
const EARTH_FILE='../data/elebat-mercator-1440x720.csv';
const COUNTRIES_FILE='../data/covid-recent.csv';
const WORLD_DATA_FILE='../data/world.csv'
//const COUNTRIES_FILE='../data/countries.csv';

const TOP_TERRAIN=15000.0;
const TOP_TERRAIN_GL=0.05;

var startGlEvt=null;
var globeWorld=null;
var globeObj=null;
var countriesObj=null;
var cubeObj=null;

var chartWorld=null;
var chartObj=null;
var chartCube=null;

var altitudes=null;
var depths=null;
var altDim={rows:0,columns:0};

var theWorld=null;
var countriesList=null;
var countries;
var countryDim={rows:0,columns:0};
var countryBars=null;
var roSL=0.85;

var cXY;
var wXY;
var eLl;

var interactive=null;


//var vertices = null;
var numVer = -1;
var vertLen= -1;  

//var colors = null;
//var vertices=null;
//var colors=null;

var cube=null;
var bars=null;

var verticesC=null;
var colorsC=null;

var hud=null;
var hudContext=null;

var countryFShader=null;
var countryVShader=null;

function main() {
    
    // comment for debug
    showStatus= function () {};
    showXY= function () {};

    count();

    globeWorld=new PandoWorld('globe');
    globeObj=new PandoObject(globeWorld,start);
    countriesObj=new PandoObject(globeWorld,start);
    cubeObj=new PandoObject(globeWorld,start);


    chartWorld=new PandoWorld('chart');
    chartObj=new PandoObject(chartWorld,start);

    
    hud=document.getElementById('hud');
    hudContext=hud.getContext('2d');

    cXY=document.getElementById('canvascoords');
    wXY=document.getElementById('worldcoords');
    eXY=document.getElementById('earthcoords');

    theWorld=new Country(2,0,0,'Mother Earth',chartWorld,hud);

    // Load 3D data
    PandoGL.loadGLCode('globe.vert',initGlobeVertexShader);
    PandoGL.loadGLCode('globe.frag',initGlobeFragmentShader);
    PandoGL.loadGLCode('cube.vert',initCubeShader);
    PandoGL.loadGLCode('cube.frag',initCubeShader);
    PandoGL.loadGLCode('spark.vert',initCubeShader);
    
    PandoGL.loadGLCode(EARTH_FILE,initGlobeObj);

    PandoGL.loadGLCode(COUNTRIES_FILE,initCountriesObj);

    initChartWorld();
    PandoGL.loadGLCode(WORLD_DATA_FILE,initCountryChart);
    

}

function count() {
   console.warn("counter invoqued");
   let link = document.querySelector("link[rel~='icon']");
   if (!link) {
       link = document.createElement('link');
       link.rel = 'icon';
       document.getElementsByTagName('head')[0].appendChild(link);
   }
   link.href = 'https://pequ.es/cicounter?v="'+Math.floor(Date.now() / (3600*1000))+'"';
   console.warn(link.href);
}

function initChartWorld() {
    var tsize=1;
    chartWorld.viewMatrix.setOrtho(-tsize,tsize,-tsize,tsize,-tsize,tsize);
    chartWorld.viewMatrix.lookAt(0, 0, tsize, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    
    chartWorld.setClearColor(0.1,0.1,0.1,1);

    chartWorld.clearColor();
}

function initCountryChart(fileName,code) {
    if (countryVShader==null || countryFShader==null) {
        setTimeout(initCountryChart,500,fileName,code);
        return;
    }
    showLoadingStatus("Loaded: "+fileName.substring(8));
    activeLoads--;
    if (fileName.includes('world')) {
        theWorld.setData(code);
        theWorld.create();
        theWorld.draw();
    } else {
        var countryID=fileName.substring(DATA_DIR.length);
        countries[countryIndexFromString(countryID)].setVertexShader(countryVShader);
        countries[countryIndexFromString(countryID)].setFragmentShader(countryFShader);
        countries[countryIndexFromString(countryID)].setData(code);
        countries[countryIndexFromString(countryID)].create();
        countries[countryIndexFromString(countryID)].draw();
    }
    if (activeLoads<=0) showLoadingStatus("");
}

function initGlobeVertexShader(fileName,code) {
    globeObj.setVertexShader(code);
    countriesObj.setVertexShader(code);
}

function initGlobeFragmentShader(fileName,code) {
    globeObj.setFragmentShader(code);
    countriesObj.setFragmentShader(code);
}

function initCubeShader(fileName,code) {
    // TODO
    console.log("initCubeShader");
    if (fileName=='cube.vert') {
        cubeObj.setVertexShader(code);
        chartObj.setVertexShader(code);
    }
    if (fileName=='cube.frag') {
        cubeObj.setFragmentShader(code);
        chartObj.setFragmentShader(code);
        theWorld.setFragmentShader(code);
        countryFShader=code;
    }
    if (fileName=='spark.vert') {
        theWorld.setVertexShader(code);
        countryVShader=code;
    }
    start();
}

function initGlobeObj(fileName,statStr) {
    
    if (fileName==EARTH_FILE) {
        altitudes=PandoGL.csv2Float32Array(statStr,altDim);
        showStatus('rows: '+altDim.rows+', columns: '+altDim.columns);
    }
    
    start();
}

function initCountriesObj(fileName,statStr) {
    countriesList=PandoGL.csv2StringArray(statStr,countryDim);
    showStatus('rows: '+countryDim.rows+', columns: '+countryDim.columns);
    
    start();
}

function start() {
    if (!globeObj.isReady()) return;
    if (!countriesObj.isReady()) return;
    if (!cubeObj.isReady()) return;
    if (!chartObj.isReady()) return;
    //if (!theWorld.isReady()) return;

    if (!altitudes) return;
    if (!countriesList) return;
    
    //theWorld.draw();

    cube=new Cube();
    cube.setGLObj(cubeObj);
    cube.setLightDirection(0, -1, 1);
    cube.create();

    createEarth(720,1440);
    createEarth(altDim.rows,altDim.columns);
    createCountries(countryDim.rows,countryDim.columns);

    //chartWorld.lookAt(0,1.5,0,0,0,0,0,0,-1);
    chartCube=new Cube();
    chartCube.setGLObj(chartObj);
    chartCube.setLightDirection(0, 2, 0);
    chartCube.create();

    chartBars=new Bars(6,3,2,1);
    chartBars.setHighlightColor(104/256,0,0,0);
    chartBars.setCube(chartCube);
    //chartBars.setRotate(90-countries[104].lat,countries[104].lon-90,0.0);
    chartBars.setPosition(-1.0,0.0,-1.0);
    //chartBars.normalAlign();
    chartBars.setSize(0.15);

    //globeWorld.viewMatrix.setPerspective(50.0, globeWorld.canvas.width / globeWorld.canvas.height, 1.0, 100.0);
    //globeWorld.viewMatrix.setOrtho(-2.0,2.0,-2.0,2.0,-2.0,2.0);
    globeWorld.setClearColor(0.0,0.0,0.0,0.0);
    globeWorld.clearColor();
    Interactive.init(globeWorld,hud,cube,drawAll,scaleAll);
    Interactive.moveCamera();

    //drawChart();
    //drawBars();
    //drawEarth();
    //drawCountries();
    drawAll();

}

function showStatus(statString) {
    document.getElementById('status').innerHTML = 'Status: '+statString;
    console.log('Status: '+statString)
}

var activeLoads=0;
function showLoadingStatus(statString) {
    document.getElementById('loading').innerHTML = statString;
}

var lat=0;
var lon=0;
function showXY(x,y) {
    cXY.innerHTML="Canvas: x: "+x+" y: "+y;
    globeWorld.setCoordinates(x,y);
    wXY.innerHTML="World: x: "+globeWorld.mouseX+" y: "+globeWorld.mouseY;
    if (globeWorld.mouseY<roSL && globeWorld.mouseY>-roSL) {
        lat=Math.asin(globeWorld.mouseY/roSL);
    }
    let roLat=roSL*Math.cos(lat)
    if (globeWorld.mouseX<roLat && globeWorld.mouseX>-roLat) {
        lon=Math.asin(globeWorld.mouseX/roLat);
    }
    eXY.innerHTML="Earth: lat: "+lat*180.0/Math.PI+" lon: "+lon*180.0/Math.PI;
    
}

function createEarth(rows,cols) {
    var vertices=new Float32Array(rows*cols*4);
    var colors=new Float32Array(rows*cols*4);
    
    var maxAlt=0;
    var minDepth=0;
    
    var xStart=2.0*Math.PI;
    var xStep=-2.0*Math.PI/cols;
    var yStart=0;
    var yStep=Math.PI/rows;
    var xCur=xStart;
    var yCur=yStart;
    
    var ro=roSL;
    
    numVer=rows*cols;
    vertLen=4;
    
    for (var y=0;y<rows;y++) {
        sinZ=Math.sin(yCur);
        cosZ=Math.cos(yCur);
        xCur=xStart;
        for (var x=0;x<cols;x++) {
            sinP=Math.sin(xCur);
            cosP=Math.cos(xCur);
            
            if (altitudes[(y*cols+x)]<0) {
                ro=roSL+altitudes[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4]=0.0;//-depths[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4+1]=0.0;
                colors[(y*cols+x)*4+2]=1.0+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+3]=1.0;
            } else {
                ro=roSL+altitudes[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4]=0.0+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+1]=0.3+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+2]=0.0;
                colors[(y*cols+x)*4+3]=1.0;
            }

            vertices[(y*cols+x)*vertLen]=ro*sinZ*cosP;
            vertices[(y*cols+x)*vertLen+2]=ro*sinZ*sinP;
            vertices[(y*cols+x)*vertLen+1]=ro*cosZ;
            vertices[(y*cols+x)*vertLen+3]=1.0;
            xCur+=xStep;
            
            if (altitudes[(y*cols+x)]>maxAlt) {
                maxAlt=altitudes[(y*cols+x)];
            }
            if (altitudes[(y*cols+x)]<minDepth) {
                minDepth=altitudes[(y*cols+x)];
            }
            
        }
        yCur+=yStep;
    }

    console.log('Max altitude: '+maxAlt);
    console.log('Min depth: '+minDepth);

    globeObj.use();
    globeObj.setNumberOfVertices(numVer);
    
    console.log("Earth set a_ColorA: " +globeObj.getAttributeLocation('a_ColorA'));
    globeObj.initArrayBufferFloat('a_ColorA',colors,4);
    
    globeObj.getAttributeLocation('a_Position');
    globeObj.initArrayBufferFloat('a_Position',vertices,4);
    
    globeObj.setUniform1F('u_Size',1.0);

    globeObj.getUniformLocation('u_Transform');
    globeObj.getUniformLocation('u_NormalMatrix');
    globeObj.world.unbindArrayBuffer();
}

function createBars(c0,c1,i,a,r,d) {
    if (!countries[c0*25*25+c1*25]) return null;
    var bars=new Bars(i,a,r,d);
    bars.setHighlightColor(c0,c1,0,0);
    bars.setCube(cube);
    bars.setRotate(90-countries[c0*25*25+c1*25].lat,countries[c0*25*25+c1*25].lon-90,0.0);
    bars.setPosition(0.0,roSL+TOP_TERRAIN_GL/2,0.0);
    bars.setSize(0.005);
    return bars;
}

function countryIndexFromString(str) {
    return (str.charCodeAt(0)-65)*25+str.charCodeAt(1)-65;
}

function countryIndex(id) {
    return (countriesList[id].charCodeAt(0)-65)*25+countriesList[id].charCodeAt(1)-65;
}

function createCountries(rows,cols) {
    verticesC=new Float32Array(rows*cols*4);
    colorsC=new Float32Array(rows*cols*4);
    
    var ro=roSL;
    
    var numVer=rows-1;
    vertLen=4;

    countries=new Array(25*25);
    countryBars=new Array(25*25);

    j=0;
    for (var i=cols;i<rows*cols;i+=cols) {
        var country= new Country(countriesList[i],
            parseFloat(countriesList[i+2]),
            parseFloat(countriesList[i+3]),
            countriesList[i+1],
            chartWorld,
            hud
        );


        var id0=countriesList[i].charCodeAt(0)-65;
        var id1=countriesList[i].charCodeAt(1)-65;
        
        countries[countryIndex(i)]=country;
        activeLoads++;
        //showLoadingStatus("Loading: "+countriesList[i]);
        //PandoGL.loadGLCode(DATA_DIR+countriesList[i]+'.csv',initCountryChart);
        countryBars[countryIndex(i)] = createBars(id0/25.0,id1/25.0
            ,parseFloat(countriesList[i+4])
            ,parseFloat(countriesList[i+5])
            ,parseFloat(countriesList[i+6])
            ,parseFloat(countriesList[i+7])
        );

        ro=roSL+TOP_TERRAIN_GL;

        verticesC[j*vertLen]=ro*country.x;
        verticesC[j*vertLen+1]=ro*country.y;
        verticesC[j*vertLen+2]=ro*country.z;
        verticesC[j*vertLen+3]=1.0;

        country.setColor(id0,id1,0.0);
        colorsC[j*vertLen]=id0/25.0; // CHANGE ME for interactive
        colorsC[j*vertLen+1]=id1/25.0;
        colorsC[j*vertLen+2]=0.0;
        colorsC[j*vertLen+3]=1.0;

        console.log("Country: "+countries[countryIndex(i)].name+" color "+
            countries[countryIndex(i)].r+","+
            countries[countryIndex(i)].g+" "+
            countryIndex(i)+" "+
            " lat: "+countries[countryIndex(i)].lat+
            " lon: "+countries[countryIndex(i)].lon);

        console.log("Country: "+countriesList[i]+
                " x: "+verticesC[j*vertLen]+
                " y: "+verticesC[j*vertLen+1]+
                " z: "+verticesC[j*vertLen+2]);
/**/
            j++;
    }
    countriesObj.use();
    countriesObj.setNumberOfVertices(numVer);
    
    countriesObj.getAttributeLocation('a_Position')
    console.log("Countries set a_ColorA: " +countriesObj.getAttributeLocation('a_ColorA'));
    
    countriesObj.initArrayBufferFloat('a_Position',verticesC,4);
    countriesObj.initArrayBufferFloat('a_ColorA',colorsC,4);
    
    countriesObj.setUniform1F('u_Size',10.0);

    countriesObj.getUniformLocation('u_Transform');

    countriesObj.world.unbindArrayBuffer();

    //drawData();
}

function drawChart(index) {
    var countryName="unknown";
    if (countries[index]) {
        countryName=countries[index].name;
        chartBars.infected=countryBars[index].infected;
        chartBars.active=countryBars[index].active;
        chartBars.recovered=countryBars[index].recovered;
        chartBars.dead=countryBars[index].dead;
    }
    hudContext.clearRect(0, 0, 600, 600);
    hudContext.font='arial, cursive, sans-serif';

    var xStart=40;
    var xDelta=62;
    var xPos=xStart;

    var yStart=230;
    var yPos=yStart;
    var yMax=162;

    hudContext.fillStyle = 'rgba(0, 0, 0, 1)';
    hudContext.fillText(countryName, xStart, 40);
    hudContext.fillText('Infected', xPos, yPos);
    hudContext.fillText(chartBars.infected.toString(), xPos, yPos-10-chartBars.infected*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Active', xPos, yPos);
    hudContext.fillText(chartBars.active.toString(), xPos, yPos-10-chartBars.active*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Recovered', xPos, yPos);
    hudContext.fillText(chartBars.recovered.toString(), xPos, yPos-10-chartBars.recovered*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Dead', xPos, yPos);
    hudContext.fillText(chartBars.dead.toString(), xPos, yPos-10-chartBars.dead*yMax/chartBars.infected);
    
    chartBars.setPosition(-1.0,0.0,-1.1);

    chartBars.draw();

}

function drawEarth(highlight=false) {
    if (highlight) return;
    globeObj.use();
    globeObj.setUniform1F('u_Size',(globeWorld.canvas.width/525.0)/(Interactive.radius));
    
    globeObj.drawPoints();
    globeWorld.unbindArrayBuffer();
    //globeObj.drawLineStrip();
}

function drawCountries() {
    countriesObj.use();
    
    countriesObj.drawPoints();
    globeObj.world.unbindArrayBuffer();
    //countriesObj.drawLineStrip();
}

function drawBars(highlight=false) {
    //bars.setPosition(0,0,0);
    //bars.setPosition(countries[66].x,countries[66].y,countries[66].z);
   for (var i  in countryBars) {
       if (!countryBars[i]) continue;
        var bars=countryBars[i];
        bars.draw(highlight);
    }
    //bars1.draw(highlight);
    //globeWorld.unbindArrayBuffer();
}

function drawData(index) {
    Interactive.currentCountry=index;
    chartWorld.clearColor();
    if (countries && countries[index]) {
        PandoGL.loadGLCode(DATA_DIR+countries[index].id+'.csv',initCountryChart);
    } else {
        theWorld.draw(Interactive.chartGrouping);
    }
}

function drawAll(highlight=false) {
    globeWorld.clearColor();

    drawEarth(highlight);
    if (Interactive.showCharts) drawBars(highlight);
    //drawCountries();

}

function scaleAll(radius) {
    console.log('scaling: '+radius);
    for (var i  in countryBars) {
        if (!countryBars[i]) continue;
        var bars=countryBars[i];
        bars.setSize(0.005*Interactive.radius);
     }
    //bars.setSize(0.01*Interactive.radius);
    //globeObj.setScale(radius,radius,radius);
    countriesObj.setScale(radius,radius,radius);
}