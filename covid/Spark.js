// Spark.js :: a class for drawing a spark lines
// (c) 2020 Jose Alvarez

class Spark {
    constructor(world) {
         
        this.x=0.0;
        this.y=0.0;
        this.z=0.0;

        this.xScale=1.0;
        this.yScale=1.0;

        this.maxX=0.0;
        this.minX=0.0;
        
        this.maxY=0.0;
        this.minY=0.0;
        this.yRange=0.0;

        this.samples=0;
        this.deltaX=1.0;

        this.r=0.6;
        this.g=0.6;
        this.b=0.6;
        this.a=1.0;

        this.dots=null;
        this.transMatrix=new Matrix4();
        this.transMatrix.setIdentity();


        this.glObject=new PandoObject(world,null);

    }

    isReady() {
        return this.glObject.isReady();
    }

    setNumberOfSamples(samples) {
        this.samples=samples;
        this.dots=new Float32Array(samples*2);
        this.deltaX=1.0/samples;

    }

    setPosition(x,y,z) {
        this.x=x;
        this.y=y;
        this.z=z;
        //this.transMatrix.setTranslate(x,y,z);
    }

    position(x,y,z) {
        this.x+=x;
        this.y+=y;
        this.z+=z;

        //this.transMatrix.translate(x,y,z);
    }

    setScale(x,y) {
        this.xScale=x;
        this.yScale=y;
        //this.transMatrix.scale(x,y,1.0);
    }

    scale(x,y) {
        this.xScale*=x;
        this.yScale*=y;
        //this.transMatrix.scale(x,y,1.0);
    }

    setColor(r,g,b,a) {
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }

    setSample(i,value) {
        this.dots[i*2]=i*this.deltaX;
        this.dots[i*2+1]=value;
        if (value>this.maxY) this.maxY=value;
        if (value<this.minY) this.minY=value;
    }

    normalizeSamples() {
        this.yRange=this.maxY-this.minY;
        for (var i=0;i<this.samples;i++) {
            //this.dots[i*2+1]-=this.minY;
            this.dots[i*2+1]/=(this.maxY-this.minY);
            //console.log("Normalised sample: "+this.dots[i*2+1]);
        }
    }

    absYOffset() {
        return this.minY/this.yRange*this.yScale;
    }

    create() {
        this.glObject.use();
        this.glObject.setNumberOfVertices(this.samples);

        this.glObject.getAttributeLocation('a_Position');
        this.glObject.getUniformLocation('u_Transform');

        this.glObject.initArrayBufferFloat('a_Position',this.dots,2);
        this.glObject.world.unbindArrayBuffer();
    }

    draw(transMatrix=this.transMatrix) {
        this.glObject.use();
        this.glObject.transMatrix.setIdentity();

        // Calculate the model view project matrix and pass it to u_Transform
        this.glObject.transMatrix.multiply(transMatrix);
        this.glObject.transMatrix.translate(this.x,this.y,0.0);
        this.glObject.transMatrix.scale(this.xScale,this.yScale,1.0);
        this.glObject.setUniform4F('u_Color',this.r,this.g,this.b,this.a);

        this.glObject.drawLineStrip();
        this.glObject.world.unbindArrayBuffer();
    }
} 