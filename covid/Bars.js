// Bars.js :: a bar chart to display current status of covid figures
// (c) 2020 Jose Alvarez

class Bars {
    constructor(infected,active,recovered,dead) {
        this.infected=infected;
        this.active=active;
        this.recovered=recovered;
        this.dead=dead;

        this.cube=null;

        this.baseMat=new Matrix4();
        this.infectedMat=new Matrix4();
        this.activeMat=new Matrix4();
        this.recoveredMat=new Matrix4();
        this.deadMat=new Matrix4();

        this.x=0.0;
        this.y=0.0;
        this.z=0.0;

        this.rx=0.0;
        this.ry=0.0;
        this.rz=0.0;

        this.sizeX=16.0;
        this.sizeY=10.0;
        this.sizeZ=10.0;

        this.inner=1.0/1.41;

        this.size=1.0;

        this.dX=0;
        this.iX=0;

        this.iZ=0;

        this.hr=66.0/256.0;
        this.hg=0.0;
        this.hb=0.0;
        this.ha=1.0;

        this.setSize(this.size);

    }

    setCube(cube) {
        this.cube=cube;
    }

    setPosition(x,y,z) {
        this.x=x;
        this.y=y;
        this.z=z;
        
    }
        
    setRotate(x,y,z) {
        this.rx=x;
        this.ry=y;
        this.rz=z;
    }

    normalAlign() {
        var normal=new Vector3();
        normal.elements[1]=this.y;
        normal.elements[0]=this.x;
        normal.elements[2]=this.z;
        normal.normalize();
        this.setRotate(Math.acos(normal.elements[0])*180.0/Math.PI,
                Math.acos(normal.elements[1])*180.0/Math.PI,
                Math.acos(normal.elements[2])*180.0/Math.PI
            );
    }

    setSize(size) {
        this.sizeX*=(size/this.size);
        this.sizeY*=(size/this.size);
        this.sizeZ*=(size/this.size);
        this.size=size;

        // TODO
        this.dX=this.sizeX*this.inner/4;
        this.dZ=this.sizeZ*this.inner;
        
        this.iX=this.sizeX*(0.25-this.inner)/2.0;
        this.iZ=this.sizeZ*(0.75-this.inner)/2.0;
    }

    scale(size) {
        this.size*=size;
        this.dX*=size;
        this.iX*=size;
        this.iZ*=size;

    }

    setHighlightColor(r,g,b,a) {
        this.hr=r;
        this.hg=g;
        this.hb=b;
        this.ha=a;
    }

    setValues(infected,active,recovered,dead) {
        this.infected=infected;
        this.active=active;
        this.recovered=recovered;
        this.dead=dead;
    }

    calcCommon(mat) {
        mat.rotate(this.ry,0.0,1.0,0.0);
        mat.rotate(this.rx,1.0,0.0,0.0);
        mat.rotate(this.rz,0.0,0.0,1.0);
        mat.translate(this.x,this.y,this.z);
    }

    calcBase() {
        this.baseMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.baseMat);
        this.baseMat.translate(0.0,-this.size/2,0.0);
        this.baseMat.scale(this.sizeX,this.size,this.sizeZ);
    }

    calcInfected() {
        this.infectedMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.infectedMat);
        this.infectedMat.translate(this.iX,0.0,this.iZ);
        this.infectedMat.scale(this.dX,
            this.sizeY*this.inner,
            //this.size,
            this.sizeZ*this.inner);
    }
    
    calcActive() {
        this.activeMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.activeMat);
        var zSize=this.sizeZ*this.inner*this.active/this.infected;
        this.activeMat.translate(this.iX+this.dX,0.0,
            this.iZ+(this.sizeZ*this.inner-zSize)/2);
        this.activeMat.scale(this.dX,
            this.sizeY*this.inner*this.active/this.infected,
            zSize);
    }
    
    calcRecovered() {
        this.recoveredMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.recoveredMat);
        var zSize=this.sizeZ*this.inner*this.recovered/this.infected;
        this.recoveredMat.translate(this.iX+2*this.dX,0.0,
            this.iZ+(this.sizeZ*this.inner-zSize)/2);
        this.recoveredMat.scale(this.dX,
            this.sizeY*this.inner*this.recovered/this.infected,
            zSize);
    }

    calcDead() {
        this.deadMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.deadMat);
        var zSize=this.sizeZ*this.inner*this.dead/this.infected;
        this.deadMat.translate(this.iX+3*this.dX,0.0,
            this.iZ+(this.sizeZ*this.inner-zSize)/2);
        this.deadMat.scale(this.dX,this.sizeY*this.inner*this.dead/this.infected,
            zSize);
    }

    drawBase(r,g,b,a) {
        this.cube.setColor(r,g,b,a);
        this.calcBase(r,g,b,a);
        this.cube.draw(this.baseMat);
    }

    drawInfected(r,g,b,a) {
        this.cube.setColor(r,g,b,a);
        this.calcInfected();
        this.cube.draw(this.infectedMat);
    }

    drawActive(r,g,b,a) {
        this.cube.setColor(r,g,b,a);
        this.calcActive();
        this.cube.draw(this.activeMat);
    }

    drawRecovered(r,g,b,a) {
        this.cube.setColor(r,g,b,a);
        this.calcRecovered();
        this.cube.draw(this.recoveredMat);
    }

    drawDead(r,g,b,a) {
        this.cube.setColor(r,g,b,a);
        this.calcDead();
        this.cube.draw(this.deadMat);
    }

    draw(highlight=false) {
        if (highlight) {
            this.drawBase(this.hr,this.hg,this.hb,this.ha);
            this.drawInfected(this.hr,this.hg,this.hb,this.ha);
            this.drawActive(this.hr,this.hg,this.hb,this.ha);
            this.drawRecovered(this.hr,this.hg,this.hb,this.ha);
            this.drawDead(this.hr,this.hg,this.hb,this.ha);

        } else {
            this.drawBase(0.9,0.9,0.9,1.0);
            this.drawInfected(0.8,0.8,0.2,1.0);
            this.drawActive(0.8,0.2,0.2,1.0);
            this.drawRecovered(0.2,0.8,0.2,1.0);
            this.drawDead(0.1,0.1,0.1,1.0);
        }
    }


}