// Cube.js :: a class for drawing a cube of different shapes, colors and sizes
// (c) 2020 Jose Alvarez

class Cube {
    constructor() {
        this.glObject=null;
         
        this.x=0.0;
        this.y=0.0;
        this.z=0.0;

        this.rx=0.0;
        this.ry=0.0;
        this.rz=0.0;

        this.length=1.0;
        this.width=1.0;
        this.height=1.0;

        this.r=0.6;
        this.g=0.6;
        this.b=0.6;
        this.a=1.0;

        this.transMatrix=new Matrix4();

        this.lightDirection=new Vector3();

    }

    setPosition(x,y,z) {
        this.x=x;
        this.y=y;
        this.z=z;
        this.transMatrix.setTranslate(x,y,z);
    }

    position(x,y,z) {
        this.x+=x;
        this.y+=y;
        this.z+=z;
        this.transMatrix.translate(x,y,z);
    }

    rotate(x,y,z) {
        //this.glObject.transMatrix.multiply(normal);
        this.rx=x;
        this.ry=y;
        this.rz=z;
        this.transMatrix.rotate(y,0.0,1.0,0.0);
        this.transMatrix.rotate(x,1.0,0.0,0.0);
        this.transMatrix.rotate(z,0.0,0.0,1.0);
    }

    setHeight(h) {
        this.height=h;
    }

    setLength(l) {
        this.length=l;
    }

    setWidth(w) {
        this.width=w;
    }

    setSize(l,w,h) {
        this.setLength(l);
        this.setWidth(w);
        this.setHeight(h);
        this.transMatrix.setScale(this.width,this.height,this.length);
    }

    scale(size) {
        this.height*=size;
        this.length*=size;
        this.width*=size;
        this.transMatrix.scale(size,size,size);
        
    }

    setColor(r,g,b,a) {
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }

    setLightDirection(x,y,z) {
        this.lightDirection.elements[0]=x;
        this.lightDirection.elements[1]=y;
        this.lightDirection.elements[2]=z;
        this.lightDirection.normalize();
    }

    setGLObj(glObject) {
        this.glObject=glObject;
    }

    create() {
        this.glObject.use();
        this.glObject.setNumberOfVertices(Cube.indices.length);

        this.glObject.getAttributeLocation('a_Position');
        this.glObject.getAttributeLocation('a_Normal');

        this.glObject.getUniformLocation('u_Transform');
        this.glObject.getUniformLocation('u_NormalMatrix');
        this.glObject.getUniformLocation('u_LightDirection');

        this.glObject.initArrayBufferFloat('a_Position',Cube.vertices,3);
        this.glObject.initArrayBufferFloat('a_Normal',Cube.normals,3);

        this.glObject.initElementArrayBufferInt('elements',Cube.indices);

        this.glObject.world.unbindArrayBuffer();
        this.glObject.world.unbindElementArrayBuffer();
        
    }

    draw(transMatrix=this.transMatrix) {
        this.glObject.use();
        this.glObject.bindElementArrayBuffer('elements',Cube.indices);

        // Calculate the model view project matrix and pass it to u_Transform
        this.glObject.transMatrix=transMatrix;
        this.glObject.setUniform3FV('u_LightDirection',this.lightDirection);
        this.glObject.setUniform4F('u_Color',this.r,this.g,this.b,this.a);

        this.glObject.drawElementTriangles();
        this.glObject.world.unbindArrayBuffer();
        this.glObject.world.unbindElementArrayBuffer();
        //this.glObject.drawElementLines();
    }
}

Cube.vertices = new Float32Array([
    0.5, 0.5, 0.5, -0.5, 0.5, 0.5, -0.5, 0.0, 0.5,  0.5, 0.0, 0.5, // v0-v1-v2-v3 front
    0.5, 0.5, 0.5,  0.5, 0.0, 0.5,  0.5, 0.0,-0.5,  0.5, 0.5,-0.5, // v0-v3-v4-v5 right
    0.5, 0.5, 0.5,  0.5, 0.5,-0.5, -0.5, 0.5,-0.5, -0.5, 0.5, 0.5, // v0-v5-v6-v1 up
    -0.5, 0.5, 0.5, -0.5, 0.5,-0.5, -0.5, 0.0,-0.5, -0.5, 0.0, 0.5, // v1-v6-v7-v2 left
    -0.5, 0.0,-0.5,  0.5, 0.0,-0.5,  0.5, 0.0, 0.5, -0.5, 0.0, 0.5, // v7-v4-v3-v2 down
    0.5, 0.0,-0.5, -0.5, 0.0,-0.5, -0.5, 0.5,-0.5,  0.5, 0.5,-0.5  // v4-v7-v6-v5 back
]);

Cube.normals = new Float32Array([
    0.0, 0.0, 1.0,  0.0, 0.0, 1.0,  0.0, 0.0, 1.0,  0.0, 0.0, 1.0, // v0-v1-v2-v3 front
    1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0,  1.0, 0.0, 0.0, // v0-v3-v4-v5 right
    0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0,  0.0, 1.0, 0.0, // v0-v5-v6-v1 up
   -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, // v1-v6-v7-v2 left
    0.0,-1.0, 0.0,  0.0,-1.0, 0.0,  0.0,-1.0, 0.0,  0.0,-1.0, 0.0, // v7-v4-v3-v2 down
    0.0, 0.0,-1.0,  0.0, 0.0,-1.0,  0.0, 0.0,-1.0,  0.0, 0.0,-1.0  // v4-v7-v6-v5 back
 ]);
 
Cube.indices = new Uint16Array([
    0, 1, 2,   0, 2, 3,    // front
    4, 5, 6,   4, 6, 7,    // right
    8, 9,10,   8,10,11,    // up
   12,13,14,  12,14,15,    // left
   16,17,18,  16,18,19,    // down
   20,21,22,  20,22,23     // back
 ]);
