// Cube vertex shader
// (c) 2020 Jose Alvarez
attribute vec4 a_Position;
attribute vec4 a_Normal;
uniform vec4 u_Color;
uniform vec3 u_LightDirection;
uniform mat4 u_Transform;
uniform mat4 u_NormalMatrix;
varying vec4 v_Color;
void main() {
  gl_Position = u_Transform * a_Position;
  vec4 color = u_Color;
  if (color.a==0.0) {
    color.a=1.0;
    v_Color=color;
  } else {
    vec3 normal = normalize((u_NormalMatrix * a_Normal).xyz);
    float nDotL = max(dot(normal.xyz, u_LightDirection), 0.0);
    v_Color = vec4(color.rgb * nDotL + vec3(0.2), color.a);
  }
}
