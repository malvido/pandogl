// PandoObject :: WebGL object wrapper class
// (c) 2020 Jose Alvarez

class PandoObject {

    constructor(world,callback) {

        if (!world) {
            throw 'PandoObject: Invalid enclosing world.'
        }

        this.vShader=null;
        this.fShader=null;

        this.program=null;

        this.numberOfVertices=0;
        this.transMatName='u_Transform';
        
        this.world=world;
        this.readyCallback=callback;
        
        this.attributes= new Map();
        this.uniforms= new Map();
        this.arrayBuffers=new Map();
        this.elementBuffers=new Map();

        this.transMatrix=new Matrix4();
        this.normalMatrix=new Matrix4();

        this.posMatrix=new Matrix4();
        this.rotMatrix=new Matrix4();
        this.scaleMatrix=new Matrix4();

        this.posMatrix.setTranslate(0.0,0.0,0.0);
        this.rotMatrix.setRotate(90.0,0.0,1.0,0.0);
        this.scaleMatrix.setScale(1.0,1.0,1.0);
    }

    setVertexShader(vShaderStr) {
        // TODO
        //console.log(vShaderStr);
        this.vShader=this.world.loadVertexShader(vShaderStr);
        if (!this.vShader) {
            throw 'PandoObject: failed to create vertex shader.';
        }
        this.createProgram();
    }
    
    setFragmentShader(fShaderStr) {
        this.fShader=this.world.loadFragmentShader(fShaderStr);
        if (!this.fShader) {
            throw 'PandoObject: failed to create fragment shader.';
        }
        this.createProgram();
    }

    createProgram() {
        if (this.vShader && this.fShader) {
            //console.log('PandoObject: Shaders ready.')
            this.program=this.world.createProgram(this.vShader,this.fShader);
            if (!this.program) {
                this.world.deleteShader(this.vShader);
                this.world.deleteShader(this.fShader);
                throw 'PandoObject: failed to create program.';
            }
            else {
                //console.log('PandoObject: Program ready.')
                if (this.readyCallback!=null) this.readyCallback();
            }
        }
    }

    setNumberOfVertices(number) {
        this.numberOfVertices=number;
    }

    isReady() {
        if (this.program && this.vShader && this.fShader) {
            return true;
        }
        console.log("program ",this.program);
        console.log("vShader ",this.vShader);
        console.log("fShader ",this.fShader);
        return false;
    }

    getAttributeLocation(name) {
        this.attributes.set(name,this.world.getAttributeLocation(this.program,name));
        return this.attributes.get(name);
    }
    
    setAttribute1F(name,value) {
        this.world.gl.vertexAttrib1f(name,value);
    }

    setAttribute3F(name,v0,v1,v2) {
        this.world.gl.vertexAttrib3f(name,v0,v1,v2);
    }

    setAttribute4F(name,v0,v1,v2,v3) {
        this.world.gl.vertexAttrib4f(name,v0,v1,v2,v3);
    }

    setUniform3FV(name,vector) {
        this.world.gl.uniform3fv(this.getUniformLocation(name),vector.elements);
    }

    setUniform4FV(name,vector) {
        this.world.gl.uniform4fv(this.getUniformLocation(name),vector.elements);
    }

    setUniform1F(name,v) {
        this.world.gl.uniform1f(this.getUniformLocation(name),v);
    }

    setUniform4F(name,x,y,z,w) {
        this.world.gl.uniform4f(this.getUniformLocation(name),x,y,z,w);
    }
    
    getUniformLocation(name) {
        this.uniforms.set(name,this.world.getUniformLocation(this.program,name));
        return this.uniforms.get(name);
    }
    
    setTransformMatrixName(name) {
        this.transMatName=name;
    }

    initArrayBuffer(bufferName,data,vectorSize,scalarType) {
        this.arrayBuffers.set(bufferName,this.world.createArrayBuffer(data,vectorSize,scalarType));
    }
    
    initArrayBufferFloat(bufferName,data,vectorSize) {
        this.initArrayBuffer(bufferName,data,vectorSize,this.world.gl.FLOAT);
    }

    bindArrayBuffers() {
        for (let [key,value] of this.arrayBuffers) {
            //console.log("PandoObject key: "+key+" value: "+this.attributes.get(key));
            this.world.bindArrayBufferToAttribute(value,this.attributes.get(key));
        }
    }

    initElementArrayBuffer(bufferName,data,scalarType) {
        this.elementBuffers.set(bufferName,this.world.createElementArrayBuffer(data,scalarType));
    }
    // const ext = gl.getExtension('OES_element_index_uint'); to enable UNSIGNED_INT
    initElementArrayBufferInt(bufferName,data) {
        this.initElementArrayBuffer(bufferName,data,this.world.gl.UNSIGNED_SHORT);
    }

    bindElementArrayBuffer(bufferName,data) {
        this.world.bindElementArrayBuffer(this.elementBuffers.get(bufferName),data);
    }

    use() {
        this.world.useObject(this);
        this.bindArrayBuffers();
    }

    applyMatrices() {
        var finalMatrix=new Matrix4(this.world.viewMatrix);

        finalMatrix.multiply(this.transMatrix);
        this.world.gl.uniformMatrix4fv(this.uniforms.get(this.transMatName),false,finalMatrix.elements);
    
        this.normalMatrix.setInverseOf(this.transMatrix);
        this.normalMatrix.transpose();

        if (this.uniforms.has('u_NormalMatrix')) {
            this.world.gl.uniformMatrix4fv(this.uniforms.get('u_NormalMatrix'),false,this.normalMatrix.elements);
        }
    }

    drawPoints() {
        this.applyMatrices();
        this.world.gl.drawArrays(this.world.gl.POINTS,0,this.numberOfVertices);
        //this.world.unbindArrayBuffer();
    }

    drawLines() {
        this.applyMatrices();
        this.world.gl.drawArrays(this.world.gl.LINES,0,this.numberOfVertices);
    }

    drawLineStrip() {
        this.applyMatrices();
        this.world.gl.drawArrays(this.world.gl.LINE_STRIP,0,this.numberOfVertices);
    }

    drawTriangles() {
        this.applyMatrices();
        this.world.gl.drawArrays(this.world.gl.TRIANGLES,0,this.numberOfVertices);
    }
    drawElementTriangles() {
        this.applyMatrices();
        this.world.gl.drawElements(this.world.gl.TRIANGLES,this.numberOfVertices,this.world.gl.UNSIGNED_SHORT,0);
    }
    drawElementLines() {
        this.applyMatrices();
        this.world.gl.drawElements(this.world.gl.LINES,this.numberOfVertices,this.world.gl.UNSIGNED_SHORT,0);
    }
    setScale(x,y,z) {
        this.transMatrix.setScale(x,y,z);
    }
    scale(x,y,z) {
        this.transMatrix.scale(x,y,z);
    }
};
