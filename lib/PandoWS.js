// PandoWS.js :: (c) 2020 Jose Alvarez
// A WebSocket class. An RTC helper. Mostly covering p2p signaling
// OBSOLETED: refer to the main RTC project

class PandoWS {
    constructor(url) {
        this.url=url;
        this.protocols="";
        this.webSocket=null;
        this.lastError;

        this.onOpenSuccess=null;
        this.onOpenFail=null;

        this.onReadSuccess=null;
    }

    isConnected() {
        if (this.webSocket==null) return false;
        return true;
    }

    setProtocols(protocols) {
        this.protocols=protocols
    }

    setOnOpen(ok,fail) {
        this.onOpenSuccess=ok;
        this.onOpenFail=fail;
    }

    setOnRead(ok) {
        this.onReadSuccess=ok;
        if (this.webSocket!=null) {
            this.webSocket.onmessage=this.onReadSuccess;
        }
    }

    static setWebSocket(obj,server) {
        obj.webSocket=server;
        obj.webSocket.onmessage=obj.onReadSuccess;
        obj.onOpenSuccess(obj);
        console.log('WebSocket ready: '+obj.url);
    }

    abortWebSocket(err) {
        console.log('Error opening WebSocket: '+err);
        this.webSocket=null;
        this.lastError=err;
    }

    connectToServer(ok,fail) {
        console.log('connectToServer: '+this.url);
        if (ok!=null) this.onOpenSuccess=ok;
        if (fail!=null) this.onOpenFail=fail;
        let url=this.url;
        let protocols=this.protocols;
        let promise=new Promise(function(resolve,reject) {
            let server=new WebSocket(url);//,protocols);
            server.onopen=function() {
                resolve(server);
            };
            server.onerror=function(err) {
                reject(err);
            };
        });

        promise.then(server=>PandoWS.setWebSocket(this,server))
            .catch(this.onOpenFail);

        if (this.webSocket==null) return false;
        return true;
    }

    close() {
        this.webSocket.close();
        this.webSocket=null;
    }

    setReceiveFunction(callback) {
        this.webSocket.onmessage = callback;
    }

}