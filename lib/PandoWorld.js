// PandoWorld :: WebGL context wrapper class
// (c) 2020 Jose Alvarez

//"use strict";
class PandoWorld {
    
    constructor(canvasName) {
        this.gl=null
        this.canvas=null;
        this.lastError=null;

        this.viewEye=null;
        this.viewCenter=null;
        this.viewUp=null;
        this.viewMatrix=null;

        this.mouseX=0;
        this.mouseY=0;

        this.canvas = document.getElementById(canvasName);

        if (!this.canvas) {
            throw 'PandoWorld: Could not find element: '+canvasName;
        }
        this.gl = WebGLUtils.setupWebGL(this.canvas);
        if (!this.gl) {
            throw 'PandoWorld: Could not create PandoWorld object.';
        }
        this.gl.enable(this.gl.DEPTH_TEST);
        //this.gl.depthFunc(this.gl.ALWAYS);
        
        this.matSide=2;
        this.viewMatrix = new Matrix4();
        this.viewMatrix.setOrtho(-this.matSide,this.matSide,-this.matSide,this.matSide,-this.matSide,this.matSide);
        this.viewMatrix.lookAt(1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0,1.0,0.0);
        //this.viewMatrix.lookAt(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,1.0,0.0);
        //this.viewMatrix.setLookAt(20.0, 10.0, 30.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        //this.viewMatrix.setPerspective(50.0, this.canvas.width / this.canvas.height, 1.0, 100.0);
    
    }
    
    lookAt(eyeX,eyeY,eyeZ,targetX,targetY,targetZ,upX,upY,upZ) {
        this.viewMatrix.setOrtho(-this.matSide,this.matSide,-this.matSide,this.matSide,-this.matSide,this.matSide);
        this.viewMatrix.lookAt(eyeX,eyeY,eyeZ,targetX,targetY,targetZ,upX,upY,upZ);
    }

    getLastError() {
        return this.lastError;
    }
    
    enableDebug() {
        this.gl = WebGLDebugUtils.makeDebugContext(this.gl);
    }

    setCoordinates(x,y) {
        let cWidth=this.canvas.width;
        let cHeight=this.canvas.height;
        let xStep=cWidth/2;
        let yStep=cHeight/2;

        this.mouseX=-1.0+2*x/cWidth;
        this.mouseY=1.0-2*y/cHeight;

    }

    loadShader(type,source) {
        var shader=this.gl.createShader(type);
        
        if (!shader) {
            this.lastError='gl.createShader failed';
            return null;
        }
        this.gl.shaderSource(shader,source);
        this.gl.compileShader(shader);
        
        if (!this.gl.getShaderParameter(shader,this.gl.COMPILE_STATUS)) {
            this.lastError='failed to get shader compile status';
            return null;
        }
        
        return shader;
    }

    loadVertexShader(source) {
        return this.loadShader(this.gl.VERTEX_SHADER,source);
    }

    loadFragmentShader(source) {
        return this.loadShader(this.gl.FRAGMENT_SHADER,source);
    }

    deleteShader(shader) {
        this.gl.deleteShader(shader);
    }

    createProgram(vShader,fShader) {
        var program=this.gl.createProgram();
        
        if (!program) {
            this.lastError='gl.createProgram failed';
            return null;
        }
        
        this.gl.attachShader(program,vShader);
        this.gl.attachShader(program,fShader);
        
        this.gl.linkProgram(program);
        
        if (!this.gl.getProgramParameter(program,this.gl.LINK_STATUS)) {
            this.lastError=gl.getProgramInfoLog(program);
            this.gl.deleteProgram(program);
            return null;
        }
        //console.log('PandoWorld: Attached shaders: '+this.gl.getProgramParameter(program,this.gl.ATTACHED_SHADERS));
        //console.log('PandoWorld: Active attributes: '+this.gl.getProgramParameter(program,this.gl.ACTIVE_ATTRIBUTES));
        return program;
    }
    
    setClearColor(r,g,b,a) {
        this.gl.clearColor(r,g,b,a);
    }

    clearColor() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT |this.gl.DEPTH_BUFFER_BIT);
    }

    getAttributeLocation(program,name) {
        return this.gl.getAttribLocation(program,name);
    }

    getUniformLocation(program,name) {
        return this.gl.getUniformLocation(program,name);
    }

    useObject(object) {
        this.gl.useProgram(object.program);
    }

    createBuffer(data,bufferType,vectorSize,scalarType) {
        var buffer=this.gl.createBuffer();
        if (!buffer) {
            this.lastError='could not create buffer';
            return null;
        }
        
        this.gl.bindBuffer(bufferType,buffer);
        this.gl.bufferData(bufferType,data,this.gl.STATIC_DRAW);
        
        buffer.vectorSize=vectorSize;
        buffer.scalarType=scalarType;
        
        return buffer;
    }

    createArrayBuffer(data,vectorSize,scalarType) {
        return this.createBuffer(data,this.gl.ARRAY_BUFFER,vectorSize,scalarType);
    }

    bindBufferToAttribute(bufferType,buffer,attribute) {
        //console.log("PandoWorld bindBufferToAttribute: "+attribute);
        this.gl.bindBuffer(bufferType,buffer);
        this.gl.vertexAttribPointer(attribute,buffer.vectorSize,buffer.scalarType,false,0,0);
        this.gl.enableVertexAttribArray(attribute);
    }

    bindArrayBufferToAttribute(buffer,attribute) {
        this.bindBufferToAttribute(this.gl.ARRAY_BUFFER,buffer,attribute);
    }

    createElementArrayBuffer(data,scalarType) {
        return this.createBuffer(data,this.gl.ELEMENT_ARRAY_BUFFER,1,scalarType);
    }

    bindElementArrayBuffer(buffer,indices) {
        
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, buffer);
        this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, indices, this.gl.STATIC_DRAW);
        
    }

    unbindBuffer(bufferType) {
        this.gl.bindBuffer(bufferType,null);
    }

    unbindArrayBuffer() {
        this.unbindBuffer(this.gl.ARRAY_BUFFER);
    }
    
    unbindElementArrayBuffer() {
        this.unbindBuffer(this.gl.ELEMENT_ARRAY_BUFFER);
    }

    dispatchEvent(event) {
        this.canvas.dispatchEvent(event);
    }
};
