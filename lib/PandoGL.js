// PandoGL :: WebGL wrapper class
// (c) 2020 Jose Alvarez
class PandoGL {
    static loadGLCode(fileName,callBack) {
        var req = new XMLHttpRequest();
        req.onreadystatechange=  function() {
            console.warn('Request ready:'+req.readyState+'\nRequest status: '+req.status)
            if (req.readyState==4) {
                if (req.status==200) {
                    console.warn(fileName);
                    callBack(fileName,req.responseText)
                    //console.warn(req.responseText);
                }
                else {
                    console.error("Error, get status: "+req.status);
                    callBack(fileName,null);
                }
            }
        }
        req.open('GET',fileName,true);
        req.send();

        //console.log(req.getAllResponseHeaders())
    }

    static saveGLCode(fileName,fileContent) {
        var blob=new Blob([fileContent],{type: 'text/plain'});
        var a=document.createElement('a');
        a.download=fileName+'.csv';
        var url=window.URL.createObjectURL(blob);
        a.href=url;
        a.textContent = 'Download ready';
        a.click();
    }

    static csv2Float32Array(longString,arrayDim=null) {
        var rows=longString.split('\n');
        if (rows.length<=1) rows=longString.split('\r');
        var rowNum=rows.length;
        var columns=rows[0].split(',');
        var colNum=columns.length;

        // omit last line if empty
        var lastColumns=rows[rowNum-1].split(',');
        if (lastColumns.length<colNum) {
            rowNum--;
        }

        if (arrayDim!=null) {
            arrayDim.rows=rowNum;
            arrayDim.columns=colNum;
        }

        var retVal=new Float32Array(rowNum*colNum)

        var i=0;
        var arrPos=0;
        for (var i=0;i<rowNum;i++) {
            columns=rows[i].split(',');
            for (var j=0;j<colNum;j++) {
                retVal[arrPos]=parseFloat(columns[j]);
                arrPos++;
            }
        }

        return retVal;
    }

    static float32Array2Csv(array,cols,round=false) {
        var csvStr='';

        var col=0;
        for (var i in array) {
            if (round)  csvStr+=Math.round(array[i]).toString();
            else csvStr+=array[i].toString();
            col++;
            if (col==cols) {
                csvStr+='\r\n';
                col=0;
            } else {
                csvStr+=',';
            }
        }
        return csvStr;
    }

    static float32Array2CsvRound(array,cols) {
        return float32Array2Csv(array,cols,true);
    }

    static csv2StringArray(longString,arrayDim=null) {
        var rows=longString.split('\n');
        var rowNum=rows.length;
        var columns=rows[0].split(',');
        var colNum=columns.length;

        // omit last line if empty
        var lastColumns=rows[rowNum-1].split(',');
        if (lastColumns.length<colNum) {
            rowNum--;
        }

        if (arrayDim!=null) {
            arrayDim.rows=rowNum;
            arrayDim.columns=colNum;
        }

        var retVal=new Array(rowNum*colNum)

        var i=0;
        var arrPos=0;
        for (var i=0;i<rowNum;i++) {
            columns=rows[i].split(',');
            for (var j=0;j<colNum;j++) {
                retVal[arrPos]=columns[j];
                arrPos++;
            }
        }

        return retVal;
    }
};
