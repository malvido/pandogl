// PandoRTC.js :: (c) 2020 Jose Alvarez
// A WebRTC wrapper class using PandoWS signaling
// OBSOLETED: refer to the main RTC project

const peerTypes = {
    UNDETERMINED: 0,
    SUPERVISOR: 1,
    VOLUNTEER: 2
}

const signalMessageTypes = {
    OK: 0,
    ERROR: 1,
    HAIL: 3,
    BYE: 4,
    NEWVOLUNTEER: 101,
    VOLUNTEERSDP: 102,
    REQUESTSDP: 3,
    SUPERVISORSDP: 5,
    ACCEPTVOLUNTEER: 10
}

var stunners=[
    "stun.l.google.com:19302",
    "stun1.l.google.com:19302",
    "stun2.l.google.com:19302",
    "stun3.l.google.com:19302",
    "stun4.l.google.com:19302",
    "stun.ekiga.net",
    "stun.ideasip.com",
    "stun.rixtelecom.se",
    "stun.schlund.de",
    "stun.stunprotocol.org:3478",
    "stun.voiparound.com",
    "stun.voipbuster.com",
    "stun.voipstunt.com",
    "stun.voxgratia.org"
  ];

class PandoRTC {
    constructor() {
        id=null;
        type=peerTypes.UNDETERMINED;

        signalingURL=null;
        signalingChannel=null;

        ice={ iceServers: [ {url: ''} ] };
        for (i=0;i<stunners.length;i++) ice.iceservers[i].urls="stun:"+stunners[i];

        peerConnections=new Map();
    }

    static CreateSupervisor(id) {
        rtcObject=new PandoRTC();
        if (rtcObject==null) return null;
        this.id=id;
        rtcObject.type=peerTypes.SUPERVISOR;

        return rtcObject;
    }

    static CreateVolunteer() {
        rtcObject=new PandoRTC();
        rtcObject.type=peerTypes.VOLUNTEER;

        return rtcObject;
    }

    static onSignalOpen(wsObj,rtcObj) {
        rtcObj.signalingChannel=wsObj;
    }

    static onSignalOpenFail(err) {
        log.error(err);
    }

    static onSignalRead(evt,rtcObj) {
        let message=rtcObj.validateSignalEvent(evt);
        if (message==null) {
            console.error('invalid signal event');
        }
        switch(rtcObj.type) {
            case peerTypes.SUPERVISOR: {
                rtcObj.supervisorSignalEvent(message);
            }
            case peerTypes.VOLUNTEER: {
                rtcObj.volunteerSignalEvent(message);
            }
            case peerTypes.UNDETERMINED: {
                console.error('undetermined peer type');
            }
            default:{
                console.error('invalid peer type');
                rtcObj.closeSignalingChannel();
            }
        }

    }

    setSignalingChannel(url) {
        if (url==null) return false;
        if (this.signalingChannel!=null) {
            console.error("signaling channel already set, close before re-setting")
        }
        this.signalingURL=url;
        let signalingChannel=new PandoWS(this.signalingURL);
        signalingChannel.setOnOpen(wsObj=>PandoRTC.onSignalOpen(wsObj,this),PandoRTC.onSignalOpenFail)
        signalingChannel.setOnRead(event=>PandoRTC.onSignalRead(event,this));
        signalingChannel.connectToServer();
    }

    closeSignalingChannel() {
        if (this.signalingChannel==null) return false;
        this.signalingChannel.close();
        this.signalingChannel=null;
        return true;
    }

    validateSignalEvent(evt) {
        let message=null;
        try {
            message=JSON.parse(evt.data);
        } catch(err) {
            console.error("JSON parsing error");
            return null;
        }
        if (message.userid!=this.id) return null;
        if (message.messagetype==null) return null;
        return message;
    }

    supervisorSignalEvent(message) {

    }

    volunteerSignalEvent(message) {

    }

    createProtoPeer() {
        let peerCon=new RTCPeerConnection(ice);
        peerCon.onicecandidate=fuction(evt) {
            if (evt.target.iceGatheringState=="complete") {
                local.createOffer(function(offer) {
                    console.log("Session Description: "+offer.sdp);
                })
            }
        }
    }

}