// vertex shader for dependency lines
// (c) 2021 Jose Alvarez

attribute vec4 a_Position;
attribute vec4 a_Color;
uniform float u_Size;
uniform mat4 u_Transform;
varying vec4 v_Color;
void main() {
    gl_Position=u_Transform*a_Position;
    gl_PointSize=u_Size;
    v_Color=a_Color;
}
