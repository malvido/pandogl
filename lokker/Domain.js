// Domain.js :: a class to hold site information
// (c) 2021 Lokker

class Domain {
    constructor(enrichedData) {
        if (enrichedData.origin) { // this is the domain object of the report
            this.skipRender=true;
            this.domain=enrichedData.origin;
            this.rootDomain=enrichedData.firstPartyDomain;
            this.lat=0;
            this.lon=0;
        
            this.altitude=0;

            this.layer=0;
            this.parents=[];
            this.children=[];
            return;
        }
        this.skipRender=false;
        this.domain=enrichedData.domain;
        this.rootDomain=enrichedData.rootDomain;
        this.category=enrichedData.category;
        this.ip=enrichedData.ipAddress;


        this.creationDate=enrichedData.whois.creationDate;
        this.registrantOrganisation=enrichedData.whois.registrantOrganisation;
        this.domainAgeDays=enrichedData.whois.domainAgeDays;

        this.city=enrichedData.geo.city;
        this.country=enrichedData.geo.country;
        this.countryCode=enrichedData.geo.countryCode;
        if (this.lat=enrichedData.geo.geoPoint) {
            this.lat=enrichedData.geo.geoPoint.lat;
            this.lon=enrichedData.geo.geoPoint.lon;
        } else {
            this.lat=0;
            this.lon=0;
        }
        this.altitude=0;

        this.layer=0;
        this.parents=[];
        this.children=[];
    }

    setColor(r,g,b,a=255) {
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }

    calcXYZ() {
        var lat=(90.0-this.lat);
        var Z=lat*Math.PI/180;
        var sinZ=Math.sin(Z);
        var cosZ=Math.cos(Z);

        var lon=(this.lon-90);
        var P=lon*Math.PI/180;
        var sinP=Math.sin(P);
        var cosP=Math.cos(P);

        this.x=sinZ*sinP;
        this.y=cosZ;
        this.z=sinZ*cosP;
    }

    addParent(parent) {
        this.parents.push(parent);
        if (this.layer<=parent.layer) this.layer=parent.layer+1;
    }

    addChild(child) {
        this.children.push(child);
    }

}