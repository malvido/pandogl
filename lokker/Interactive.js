// Interactive.js :: a 3d lokker data console helper class
// (c) 2021 Jose Alvarez

class Interactive {

    static init(world,hud,cube,drawFunction,scaleFunction) {
        Interactive.world=world;
        Interactive.cube=cube;
        Interactive.canvas=world.canvas;
        Interactive.hud=hud;
        Interactive.drawAll=drawFunction;
        Interactive.scaleObjects=scaleFunction;
        
        //Interactive.hudContext=hud.getContext('2d');
        //Interactive.drawHudContext();

        Interactive.canvas.onmousedown=Interactive.mouseDown;
        Interactive.canvas.onmouseup=Interactive.mouseUp;
        Interactive.canvas.onmousemove= Interactive.mouseMove;
        Interactive.canvas.onmouseout= Interactive.mouseUp;

        Interactive.canvas.addEventListener('wheel',Interactive.wheelScroll,true);

        // Touch event compatibility for mobile devices
        Interactive.canvas.addEventListener('touchmove',Interactive.touchMove);
        Interactive.canvas.addEventListener("touchstart", Interactive.touchStart, false);
        Interactive.canvas.addEventListener("touchend",  Interactive.touchEnd, false);

   
        //Interactive.initHudContext();
        var tick=function() {
            if (Interactive.autoSpin) {
                Interactive.bearing-=Interactive.bearingStep/4;
                Interactive.moveCamera();
            }
            Interactive.drawAll();
            
            requestAnimationFrame(tick);
        };
        tick();
    }

    static drawHudContext() {
        Interactive.hudContext.clearRect(0, 0, 600, 600);
        Interactive.hudContext.fillStyle = 'rgba(255, 255, 0, 1)';
        Interactive.hudContext.font='24px arial, cursive, sans-serif';
        Interactive.hudContext.fillText('Report: Data', 10, 20);

    }

    static colorToCountry(c0,c1) {
        return Math.round(c0*25/255)*25+Math.round(c1*25/255);
    }

    static createHover(domainData) {
        let hoverText=""
        if (domainData) {
            hoverText+=`<b>${domainData.rootDomain}</b><br>`;
            hoverText+=`${domainData.domain}<br>`;
            if (domainData.ip) hoverText+=`IP: ${domainData.ip}<br>`;
            if (domainData.creationDate) hoverText+=`Created: ${domainData.creationDate}<br>`;
            if (domainData.country) {
                hoverText+=`Country: ${domainData.country}<br>`;
            }
            if (domainData.city) {
                hoverText+=`City: ${domainData.city}<br>`;
            }
            if (domainData.category) hoverText+=`Category: ${domainData.category}<br>`;
            hoverText+=`Dependency level: ${domainData.altitude}<br>`;
        }
        else return null;
        return hoverText;
    }

    static mouseDown(event) {
        var pixels = new Uint8Array(4);
        pixels[0]=1;
        Interactive.canvas.style.cursor="move";
        Interactive.moveActive=true;
        //Interactive.cube.setLightDirection(0, 2, 0,);
        Interactive.drawAll(true);
        //drawCountries();
        Interactive.world.gl.readPixels(event.offsetX,Interactive.canvas.width-event.offsetY,1,1,
            Interactive.world.gl.RGBA,Interactive.world.gl.UNSIGNED_BYTE,pixels);
        //var countryIndex=Interactive.colorToCountry(pixels[0],pixels[1])
        console.log(`${pixels[0]},${pixels[1]},${pixels[2]}`);
        //console.log(domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])]);
        //console.log(domainObjects[domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])].domain]);

        //let domainDescription=Interactive.createHover(domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])]);
        if (!domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])]) return;
        let domainDescription=Interactive.createHover(domainObjects[domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])].domain]);
        if (domainDescription) {
            console.log(domainObjects[domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])].domain]);
            console.log(domainObjects[domainColorMap[Interactive.RGB2HTML(pixels[0],pixels[1],pixels[2])].domain]);
            document.getElementById("domData").innerHTML = domainDescription;
            //Interactive.hudContext.fillText('Report: Data', 10, 20);
            //Interactive.hudContext.fillText(domainDescription, 10, 40);

        }
    }

    static mouseUp(event) {
        Interactive.moveActive=false;
        Interactive.canvas.style.cursor="initial";
    }

    static mouseMove(event) {
        showXY(event.offsetX,event.offsetY);
        if (!Interactive.moveActive) return;
        Interactive.azimut+=event.movementY*Interactive.azimutStep;
        if (Interactive.azimut<-Math.PI/2) Interactive.azimut=-Math.PI/2;
        if (Interactive.azimut>Math.PI/2) Interactive.azimut=Math.PI/2;
        Interactive.bearing-=event.movementX*Interactive.bearingStep;
        Interactive.moveCamera();
    }

    static touchStart(event) {
        Interactive.lastTouchX=-1;
        Interactive.lastTouchY=-1;
        Interactive.lastTouchDistance=-1;
    }

    static touchMove(event) {
        var touches = event.changedTouches;
        event.preventDefault();

        // zoom
        if (touches.length>1) {
            var x2=(touches[0].pageX-touches[1].pageX)*(touches[0].pageX-touches[1].pageX);
            var y2=(touches[0].pageY-touches[1].pageY)*(touches[0].pageY-touches[1].pageY);
            
            var currentDistance=Math.sqrt(x2+y2)
            if (Interactive.lastTouchDistance<0) Interactive.lastTouchDistance=currentDistance;
            Interactive.zoom(Interactive.lastTouchDistance-currentDistance);
            Interactive.lastTouchDistance=currentDistance;
        }
        // drag
        else {
            if (Interactive.lastTouchDistance>=0) return; // still zooming
            if (Interactive.lastTouchX<0) Interactive.lastTouchX=touches[0].pageX;
            if (Interactive.lastTouchY<0) Interactive.lastTouchY=touches[0].pageY;
            var movementX=(touches[0].pageX-Interactive.lastTouchX);
            var movementY=(touches[0].pageY-Interactive.lastTouchY);

            Interactive.azimut+=movementY*Interactive.azimutStep;
            if (Interactive.azimut<-Math.PI/2) Interactive.azimut=-Math.PI/2;
            if (Interactive.azimut>Math.PI/2) Interactive.azimut=Math.PI/2;
            Interactive.bearing-=movementX*Interactive.bearingStep;
            Interactive.moveCamera();

            Interactive.lastTouchX=touches[0].pageX;
            Interactive.lastTouchY=touches[0].pageY;

            //console.log("Touch x: "+touches[0].pageX+","+touches[0].pageY);
        }

    }

    static touchEnd(event) {
        Interactive.lastTouchX=-1;
        Interactive.lastTouchY=-1;
        Interactive.lastTouchDistance=-1;
    }

    static zoom(delta) {
        Interactive.radius+=Interactive.radiusStep*delta;

        if (Interactive.radius<0.48) Interactive.radius=0.48;
        if (Interactive.radius>1.0) Interactive.radius=1.0;
        showStatus("radius: "+Interactive.radius);

        Interactive.scaleObjects(Interactive.radius);

        Interactive.moveCamera();
    }

    static wheelScroll(event) {
        Interactive.zoom(event.deltaY); 
        event.preventDefault();
        return false;
    } 

    static moveCamera() {
        Interactive.world.viewMatrix.setOrtho(-Interactive.radius,Interactive.radius,-Interactive.radius,Interactive.radius,-Interactive.radius,Interactive.radius);
        var camX=Interactive.radius*Math.sin(Interactive.bearing)*Math.cos(Interactive.azimut);
        var camY=Interactive.radius*Math.sin(Interactive.azimut);
        var camZ=Interactive.radius*Math.cos(Interactive.bearing)*Math.cos(Interactive.azimut);
        
        Interactive.world.viewMatrix.lookAt(camX, camY, camZ,
                            0.0, 0.0, 0.0, 
                            0.0,1.0,0.0);
        Interactive.cube.setLightDirection(camX, camY, camZ,);
    }

    static motherEarth() {
        drawData();
    }
    static toggleBars() {
        if (Interactive.showCharts==true) Interactive.showCharts=false;
        else Interactive.showCharts=true;
    }
    static toggleSpin() {
        if (Interactive.autoSpin==true) Interactive.autoSpin=false;
        else Interactive.autoSpin=true;
    }
    static toggleGrouping() {
        if (Interactive.chartGrouping==true) Interactive.chartGrouping=false;
        else Interactive.chartGrouping=true;
        drawData(Interactive.currentCountry);
    }

    static RGB2HTML(red, green, blue)
    {
        var decColor = red + 256 * green + 65536 * blue;
        return decColor.toString(16);
    }
}

// Static properties: not very readable but more compatible (Firefox)
Interactive.canvas=null;
Interactive.world=null;
Interactive.hud=null;
Interactive.hudContext=null;
Interactive.cube=null;
Interactive.azimut=0.0;
Interactive.azimutStep=0.01;
Interactive.bearing=0.0;
Interactive.bearingStep=0.01;
Interactive.radius=1.0;
Interactive.radiusStep=0.005;
Interactive.moveActive=false;
Interactive.drawAll=null;
Interactive.scaleObjects=null;
Interactive.autoSpin=true;
Interactive.showCharts=true;
Interactive.currentCountry;
Interactive.chartGrouping=false;
Interactive.lastTouchX=-1;
Interactive.lastTouchY=-1;
Interactive.lastTouchDistance=-1;