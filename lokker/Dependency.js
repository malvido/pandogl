// Dependency.js :: a class for managing dependencies on a globe
// (c) 2021 :: Lokker

class Dependency {
    constructor(parent,child) {
        console.log("Dependency Parent: "+parent.domain
            +" Child: "+child.domain);
        this.parent=parent;
        this.child=child;

        this.parent.addChild(child);
        this.child.addParent(parent);
    }
}