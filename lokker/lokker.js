// lokker.js :: a 3d lokker data console 
// (c) 2021 Jose Alvarez

const DATA_DIR='../data/'
const EARTH_FILE='../data/elebat-mercator-1440x720.csv';
const COUNTRIES_FILE='../data/covid-recent.csv';
const WORLD_DATA_FILE='../data/world.csv'
//const COUNTRIES_FILE='../data/countries.csv';
REPORT_FILE='./reports/teradyne.json';
//REPORT_FILE='./reports/acm.json';
//REPORT_FILE='./reports/tmz.json';
//REPORT_FILE='./reports/lifehacker.json';

const TOP_TERRAIN=15000.0;
const TOP_TERRAIN_GL=0.05;

var startGlEvt=null;
var globeWorld=null;
var globeObj=null;
var countriesObj=null;
var cubeObj=null;
var flatCubeObj=null;
var dependencyObj=null;

var chartWorld=null;
var chartObj=null;
var chartCube=null;

var altitudes=null;
var depths=null;
var altDim={rows:0,columns:0};

var theWorld=null;
var countriesList=null;
var countries;
var countryDim={rows:0,columns:0};
var countryBars=null;
var roSL=0.85;

var report=null;
var domainsList=null;
var domainObjects={};
var domainColorMap={};
var dependenciesList=null;

var cXY;
var wXY;
var eLl;

var interactive=null;


//var vertices = null;
var numVer = -1;
var vertLen= -1;  

//var colors = null;
//var vertices=null;
//var colors=null;

var cube=null;
var bars=null;

var verticesC=null;
var colorsC=null;

var hud=null;
var hudContext=null;

var countryFShader=null;
var countryVShader=null;

function main() {
    
    // comment for debug
    showStatus= function () {};
    showXY= function () {};

    //count();

    globeWorld=new PandoWorld('globe');
    globeObj=new PandoObject(globeWorld,start);
    countriesObj=new PandoObject(globeWorld,start);
    cubeObj=new PandoObject(globeWorld,start);
    flatCubeObj=new PandoObject(globeWorld,start);
    dependencyObj=[] //new PandoObject(globeWorld,start);

    chartWorld=new PandoWorld('chart');
    chartObj=new PandoObject(chartWorld,start);

    
    hud=document.getElementById('hud');
    //hudContext=hud.getContext('2d');

    cXY=document.getElementById('canvascoords');
    wXY=document.getElementById('worldcoords');
    eXY=document.getElementById('earthcoords');

    //theWorld=new Country(2,0,0,'Mother Earth',chartWorld,hud);

    // Load 3D data
    PandoGL.loadGLCode('globe.vert',initGlobeVertexShader);
    PandoGL.loadGLCode('globe.frag',initGlobeFragmentShader);
    PandoGL.loadGLCode('cube.vert',initCubeShader);
    PandoGL.loadGLCode('flatcube.vert',initCubeShader);
    PandoGL.loadGLCode('cube.frag',initCubeShader);
    PandoGL.loadGLCode('dependency.vert',initCubeShader);
    
    PandoGL.loadGLCode(EARTH_FILE,initGlobeObj);

    //PandoGL.loadGLCode(COUNTRIES_FILE,initCountriesObj);

    //PandoGL.loadGLCode(REPORT_FILE,initReport);

    initChartWorld();
    //PandoGL.loadGLCode(WORLD_DATA_FILE,initCountryChart);
    

}

function count() {
   console.warn("counter invoked");
   let link = document.querySelector("link[rel~='icon']");
   if (!link) {
       link = document.createElement('link');
       link.rel = 'icon';
       document.getElementsByTagName('head')[0].appendChild(link);
   }
   link.href = 'https://pequ.es/cicounter?v="'+Math.floor(Date.now() / (3600*1000))+'"';
   console.warn(link.href);
}

function initChartWorld() {
    var tsize=1;
    chartWorld.viewMatrix.setOrtho(-tsize,tsize,-tsize,tsize,-tsize,tsize);
    chartWorld.viewMatrix.lookAt(0, 0, tsize, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    
    chartWorld.setClearColor(0.1,0.1,0.1,1);

    chartWorld.clearColor();
}

function initCountryChart(fileName,code) {
    if (countryVShader==null || countryFShader==null) {
        setTimeout(initCountryChart,500,fileName,code);
        return;
    }
    showLoadingStatus("Loaded: "+fileName.substring(8));
    activeLoads--;
    if (fileName.includes('world')) {
        //theWorld.setData(code);
        //theWorld.create();
        //theWorld.draw();
    } else {
        var countryID=fileName.substring(DATA_DIR.length);
        countries[countryIndexFromString(countryID)].setVertexShader(countryVShader);
        countries[countryIndexFromString(countryID)].setFragmentShader(countryFShader);
        countries[countryIndexFromString(countryID)].setData(code);
        countries[countryIndexFromString(countryID)].create();
        countries[countryIndexFromString(countryID)].draw();
    }
    if (activeLoads<=0) showLoadingStatus("");
}

function initGlobeVertexShader(fileName,code) {
    console.log("initGlobeVertexShader");
    globeObj.setVertexShader(code);
    countriesObj.setVertexShader(code);
}

var dependencyFrag;
function initGlobeFragmentShader(fileName,code) {
    console.log("initGlobeFragmentShader");
    globeObj.setFragmentShader(code);
    countriesObj.setFragmentShader(code);
    //dependencyObj.setFragmentShader(code);
    dependencyFrag=code;
}
var dependencyVert;
function initCubeShader(fileName,code) {
    // TODO
    console.log("initCubeShader: ",fileName);
    if (fileName=='cube.vert') {
        cubeObj.setVertexShader(code);
        chartObj.setVertexShader(code);
    }
    if (fileName=='flatcube.vert') {
        flatCubeObj.setVertexShader(code);
    }
    if (fileName=='cube.frag') {
        cubeObj.setFragmentShader(code);
        flatCubeObj.setFragmentShader(code);
        chartObj.setFragmentShader(code);
        //theWorld.setFragmentShader(code);
        countryFShader=code;
    }
    if (fileName=='dependency.vert') {
        //theWorld.setVertexShader(code);
        //dependencyObj.setVertexShader(code);
        dependencyVert=code;
        //countryVShader=code;
    }
    start();
}

function initGlobeObj(fileName,statStr) {
    
    if (fileName==EARTH_FILE) {
        altitudes=PandoGL.csv2Float32Array(statStr,altDim);
        showStatus('rows: '+altDim.rows+', columns: '+altDim.columns);
    }
    
    start();
}

function domainAltitude(domainObject) {
    if (!domainObject) return -1;
    console.log("Domain altitude: "+domainObject.domain+" "+domainObject.altitude);
    for (let i in domainObject.parents) {
        if (domainObject.domain==domainObject.parents[i].domain) continue;
        if (domainObject.parents[i].altitude>=domainObject.altitude) domainObject.altitude=domainObject.parents[i].altitude+1;
    }
    console.log("Domain altitude: "+domainObject.domain+" "+domainObject.altitude);
    return domainObject.altitude;
}

function initReport(fileName,statStr) {
    report=JSON.parse(statStr);
//console.log()
    document.getElementById("repTitle").innerHTML = report.page.firstPartyDomain;

    domainsList=[];
    let domain=new Domain(report.page);
    domain.setColor(255,domain.domain.length%255,domain.rootDomain.length%255);
    domainColorMap[Interactive.RGB2HTML(255,domain.domain.length%255,domain.rootDomain.length%255)]=domain;
    domainObjects[domain.domain]=domain;
    domainObjects[report.page.firstPartyDomain]=domain;
    domainsList.push(domain);
    for (let i=0;i<report.enrichedDomains.length;i++) {
        domain=new Domain(report.enrichedDomains[i]);
        domain.setColor(i%255,domain.domain.length%255,domain.rootDomain.length%255);
        domain.calcXYZ();
        domainColorMap[Interactive.RGB2HTML(i%255,domain.domain.length%255,domain.rootDomain.length%255)]=domain;
        domainObjects[domain.domain]=domain;
        domainsList.push(domain);
    }
    dependenciesList=[];
    for (let i=0;i<report.dependencies.flat.length;i++) {
        if (domainObjects[report.dependencies.flat[i].parent] && domainObjects[report.dependencies.flat[i].child]) {
            console.log("Processing "
                +report.dependencies.flat[i].parent
                +" "
                +report.dependencies.flat[i].child
            );
            console.log("Processing "
                +domainObjects[report.dependencies.flat[i].parent].domain
                +" "
                +domainObjects[report.dependencies.flat[i].child].domain
            );
            console.log(domainObjects[report.dependencies.flat[i].parent]);
            let dependency=new Dependency(
                domainObjects[report.dependencies.flat[i].parent]
                ,domainObjects[report.dependencies.flat[i].child]
            );
            dependenciesList.push(dependency);
        } else console.error("No object record for "+report.dependencies.flat[i].parent);
    }
    for (let j=0;j<4;j++) {
        for (let i in domainsList) {
            domainAltitude(domainsList[i]);
            if (domainsList[i].altitude<0) console.warn("Fucking altitude! "+domainsList[i].domain);
        }
    }

    showStatus('domains: '+domainsList.length+', dependencies: '+dependenciesList.length);

    createDomains();
    createDependencies(altDim.rows,altDim.columns);
    //start();
}

function start() {
    console.log("start called");
    if (!altitudes) return;
    console.log("altitudes loaded");

    if (!flatCubeObj.isReady()) return;
    if (!cubeObj.isReady()) return;
    if (!globeObj.isReady()) return;

    console.log("3d objects loaded");

    //theWorld.draw();

    cube=new Cube();
    cube.setGLObj(cubeObj);
    cube.setLightDirection(0, -1, 1);
    cube.create();

    flatCube=new FlatCube();
    flatCube.setGLObj(flatCubeObj);
    flatCube.create();

    //createEarth(720,1440);
    createEarth(altDim.rows,altDim.columns);
    /*
    createDomains();
    createDependencies(altDim.rows,altDim.columns);
    */
    //createCountries(countryDim.rows,countryDim.columns);

    
    //globeWorld.viewMatrix.setPerspective(50.0, globeWorld.canvas.width / globeWorld.canvas.height, 1.0, 100.0);
    //globeWorld.viewMatrix.setOrtho(-2.0,2.0,-2.0,2.0,-2.0,2.0);
    globeWorld.setClearColor(0.0,0.0,0.0,0.0);
    globeWorld.clearColor();
    Interactive.init(globeWorld,hud,cube,drawAll,scaleAll);
    Interactive.moveCamera();

    PandoGL.loadGLCode(REPORT_FILE,initReport);
    drawAll();

}

function showStatus(statString) {
    document.getElementById('status').innerHTML = 'Status: '+statString;
    console.log('Status: '+statString)
}

var activeLoads=0;
function showLoadingStatus(statString) {
    document.getElementById('loading').innerHTML = statString;
}

var lat=0;
var lon=0;
function showXY(x,y) {
    cXY.innerHTML="Canvas: x: "+x+" y: "+y;
    globeWorld.setCoordinates(x,y);
    wXY.innerHTML="World: x: "+globeWorld.mouseX+" y: "+globeWorld.mouseY;
    if (globeWorld.mouseY<roSL && globeWorld.mouseY>-roSL) {
        lat=Math.asin(globeWorld.mouseY/roSL);
    }
    let roLat=roSL*Math.cos(lat)
    if (globeWorld.mouseX<roLat && globeWorld.mouseX>-roLat) {
        lon=Math.asin(globeWorld.mouseX/roLat);
    }
    eXY.innerHTML="Earth: lat: "+lat*180.0/Math.PI+" lon: "+lon*180.0/Math.PI;
    
}

function createEarth(rows,cols) {
    var vertices=new Float32Array(rows*cols*4);
    var colors=new Float32Array(rows*cols*4);
    
    var maxAlt=0;
    var minDepth=0;
    
    var xStart=2.0*Math.PI;
    var xStep=-2.0*Math.PI/cols;
    var yStart=0;
    var yStep=Math.PI/rows;
    var xCur=xStart;
    var yCur=yStart;
    
    var ro=roSL;
    
    numVer=rows*cols;
    vertLen=4;
    
    for (var y=0;y<rows;y++) {
        sinZ=Math.sin(yCur);
        cosZ=Math.cos(yCur);
        xCur=xStart;
        for (var x=0;x<cols;x++) {
            sinP=Math.sin(xCur);
            cosP=Math.cos(xCur);
            
            if (altitudes[(y*cols+x)]<0) {
                ro=roSL+altitudes[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4]=0.0;//-depths[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4+1]=0.0;
                colors[(y*cols+x)*4+2]=1.0+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+3]=1.0;
            } else {
                ro=roSL+altitudes[(y*cols+x)]/TOP_TERRAIN*TOP_TERRAIN_GL;
                colors[(y*cols+x)*4]=0.0+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+1]=0.3+altitudes[(y*cols+x)]/TOP_TERRAIN;
                colors[(y*cols+x)*4+2]=0.0;
                colors[(y*cols+x)*4+3]=1.0;
            }

            vertices[(y*cols+x)*vertLen]=ro*sinZ*cosP;
            vertices[(y*cols+x)*vertLen+2]=ro*sinZ*sinP;
            vertices[(y*cols+x)*vertLen+1]=ro*cosZ;
            vertices[(y*cols+x)*vertLen+3]=1.0;
            xCur+=xStep;
            
            if (altitudes[(y*cols+x)]>maxAlt) {
                maxAlt=altitudes[(y*cols+x)];
            }
            if (altitudes[(y*cols+x)]<minDepth) {
                minDepth=altitudes[(y*cols+x)];
            }
            
        }
        yCur+=yStep;
    }
    console.log("Half way: "
        ,vertices[numVer/2*vertLen]
        ,vertices[numVer/2*vertLen+1]
        ,vertices[numVer/2*vertLen+2]
        ,vertices[numVer/2*vertLen+3]
    );
    console.log('Max altitude: '+maxAlt);
    console.log('Min depth: '+minDepth);

    globeObj.use();
    globeObj.setNumberOfVertices(numVer);
    
    console.log("Earth set a_ColorA: " +globeObj.getAttributeLocation('a_ColorA'));
    globeObj.initArrayBufferFloat('a_ColorA',colors,4);
    
    globeObj.getAttributeLocation('a_Position');
    globeObj.initArrayBufferFloat('a_Position',vertices,4);
    
    globeObj.setUniform1F('u_Size',1.0);

    globeObj.getUniformLocation('u_Transform');
    globeObj.world.unbindArrayBuffer();
}

function createBars(c0,c1,i,a,r,d) {
    if (!countries[c0*25*25+c1*25]) return null;
    var bars=new Bars(i,a,r,d);
    bars.setHighlightColor(c0,c1,0,0);
    bars.setCube(cube);
    bars.setRotate(90-countries[c0*25*25+c1*25].lat,countries[c0*25*25+c1*25].lon-90,0.0);
    bars.setPosition(0.0,roSL+TOP_TERRAIN_GL/2,0.0);
    bars.setSize(0.005);
    return bars;
}

function countryIndexFromString(str) {
    return (str.charCodeAt(0)-65)*25+str.charCodeAt(1)-65;
}

function countryIndex(id) {
    return (countriesList[id].charCodeAt(0)-65)*25+countriesList[id].charCodeAt(1)-65;
}

var domainSolids=null;
function createDomains() {
    domainSolids=new Array(domainsList.length);

    for (let i=0;i<domainsList.length;i++) {
        var domain3d=new Domain3D();
        domain3d.setHighlightColor(domainsList[i].r,domainsList[i].g,domainsList[i].b,)
        domain3d.setCube(cube);
        domain3d.setHighlightCube(flatCube);
        domain3d.setRotate(90-domainsList[i].lat,domainsList[i].lon-90,0.0);
        let basePos=roSL+TOP_TERRAIN_GL/2;
        domain3d.setPosition(0.0,basePos+basePos/20*domainsList[i].altitude,0.0);
        domain3d.setSize(0.005);

        domainSolids[i]=domain3d;
        //console.log(domain3d);
    }
}

var dependenciesSolid=null;
function createDependencies(rows,cols) {
    console.log("createDependencies: ",rows,cols,dependenciesList.length);
    dependenciesSolid=new Array(dependenciesList.length);

    for (let i=0;i<dependenciesList.length;i++) {
        let dependencyObj= new PandoObject(globeWorld,null);
        dependencyObj.setVertexShader(dependencyVert);
        dependencyObj.setFragmentShader(dependencyFrag);
        let dependency3D=new Dependency3D(rows,cols);
        dependency3D.setGLObj(dependencyObj);
        dependency3D.setDependency(dependenciesList[i]);
        dependency3D.create();
        dependenciesSolid[i]=dependency3D;
    }
}

function createCountries(rows,cols) {
    verticesC=new Float32Array(rows*cols*4);
    colorsC=new Float32Array(rows*cols*4);
    
    var ro=roSL;
    
    var numVer=rows-1;
    vertLen=4;

    countries=new Array(25*25);
    countryBars=new Array(25*25);

    j=0;
    for (var i=cols;i<rows*cols;i+=cols) {
        var country= new Country(countriesList[i],
            parseFloat(countriesList[i+2]),
            parseFloat(countriesList[i+3]),
            countriesList[i+1],
            chartWorld,
            hud
        );


        var id0=countriesList[i].charCodeAt(0)-65;
        var id1=countriesList[i].charCodeAt(1)-65;
        
        countries[countryIndex(i)]=country;
        activeLoads++;
        //showLoadingStatus("Loading: "+countriesList[i]);
        //PandoGL.loadGLCode(DATA_DIR+countriesList[i]+'.csv',initCountryChart);
        countryBars[countryIndex(i)] = createBars(id0/25.0,id1/25.0
            ,parseFloat(countriesList[i+4])
            ,parseFloat(countriesList[i+5])
            ,parseFloat(countriesList[i+6])
            ,parseFloat(countriesList[i+7])
        );

        ro=roSL+TOP_TERRAIN_GL;

        verticesC[j*vertLen]=ro*country.x;
        verticesC[j*vertLen+1]=ro*country.y;
        verticesC[j*vertLen+2]=ro*country.z;
        verticesC[j*vertLen+3]=1.0;

        country.setColor(id0,id1,0.0);
        colorsC[j*vertLen]=id0/25.0; // CHANGE ME for interactive
        colorsC[j*vertLen+1]=id1/25.0;
        colorsC[j*vertLen+2]=0.0;
        colorsC[j*vertLen+3]=1.0;

        console.log("Country: "+countries[countryIndex(i)].name+" color "+
            countries[countryIndex(i)].r+","+
            countries[countryIndex(i)].g+" "+
            countryIndex(i)+" "+
            " lat: "+countries[countryIndex(i)].lat+
            " lon: "+countries[countryIndex(i)].lon);

        console.log("Country: "+countriesList[i]+
                " x: "+verticesC[j*vertLen]+
                " y: "+verticesC[j*vertLen+1]+
                " z: "+verticesC[j*vertLen+2]);
/**/
            j++;
    }
    countriesObj.use();
    countriesObj.setNumberOfVertices(numVer);
    
    countriesObj.getAttributeLocation('a_Position')
    console.log("Countries set a_ColorA: " +countriesObj.getAttributeLocation('a_ColorA'));
    
    countriesObj.initArrayBufferFloat('a_Position',verticesC,4);
    countriesObj.initArrayBufferFloat('a_ColorA',colorsC,4);
    
    countriesObj.setUniform1F('u_Size',10.0);

    countriesObj.getUniformLocation('u_Transform');

    countriesObj.world.unbindArrayBuffer();

    //drawData();
}

function drawChart(index) {
    var countryName="unknown";
    if (countries[index]) {
        countryName=countries[index].name;
        chartBars.infected=countryBars[index].infected;
        chartBars.active=countryBars[index].active;
        chartBars.recovered=countryBars[index].recovered;
        chartBars.dead=countryBars[index].dead;
    }
    hudContext.clearRect(0, 0, 600, 600);
    hudContext.font='arial, cursive, sans-serif';

    var xStart=40;
    var xDelta=62;
    var xPos=xStart;

    var yStart=230;
    var yPos=yStart;
    var yMax=162;

    hudContext.fillStyle = 'rgba(0, 0, 0, 1)';
    hudContext.fillText(countryName, xStart, 40);
    hudContext.fillText('Infected', xPos, yPos);
    hudContext.fillText(chartBars.infected.toString(), xPos, yPos-10-chartBars.infected*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Active', xPos, yPos);
    hudContext.fillText(chartBars.active.toString(), xPos, yPos-10-chartBars.active*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Recovered', xPos, yPos);
    hudContext.fillText(chartBars.recovered.toString(), xPos, yPos-10-chartBars.recovered*yMax/chartBars.infected);
    
    xPos+=xDelta;
    hudContext.fillText('Dead', xPos, yPos);
    hudContext.fillText(chartBars.dead.toString(), xPos, yPos-10-chartBars.dead*yMax/chartBars.infected);
    
    chartBars.setPosition(-1.0,0.0,-1.1);

    chartBars.draw();

}

function drawEarth(highlight=false) {
    if (highlight) return;
    globeObj.use();
    globeObj.setUniform1F('u_Size',(globeWorld.canvas.width/525.0)/(Interactive.radius));
    
    globeObj.drawPoints();
    globeWorld.unbindArrayBuffer();
    //globeObj.drawLineStrip();
}

function drawDomains(highlight=false) {
    for (let i  in domainSolids) {
        if (!domainSolids[i]) continue;
        let domain=domainSolids[i];
        domain.draw(highlight);
     }
}

function drawDependencies(highlight=false) {
    for (let i  in dependenciesSolid) {
        if (!dependenciesSolid[i]) {
            console.warn("missing dependency");
            continue;
        }
        //dependenciesSolid[i].create();
        dependenciesSolid[i].draw(globeWorld.canvas.width/525.0)/(Interactive.radius);
     }
}
/*
function drawDependencies(highlight=false) {
    dependenciesSolid[0].create();
    dependenciesSolid[0].draw(globeWorld.canvas.width/525.0)/(Interactive.radius);
}
*/
function drawCountries() {
    countriesObj.use();
    
    countriesObj.drawPoints();
    globeObj.world.unbindArrayBuffer();
    //countriesObj.drawLineStrip();
}

function drawBars(highlight=false) {
    //bars.setPosition(0,0,0);
    //bars.setPosition(countries[66].x,countries[66].y,countries[66].z);
   for (var i  in countryBars) {
       if (!countryBars[i]) continue;
        var bars=countryBars[i];
        bars.draw(highlight);
    }
    //bars1.draw(highlight);
    //globeWorld.unbindArrayBuffer();
}

function drawData(index) {
    Interactive.currentCountry=index;
    chartWorld.clearColor();
    if (countries && countries[index]) {
        PandoGL.loadGLCode(DATA_DIR+countries[index].id+'.csv',initCountryChart);
    } else {
        //theWorld.draw(Interactive.chartGrouping);
    }
}

function drawAll(highlight=false) {
    globeWorld.clearColor();

    drawEarth(highlight);
    if (Interactive.showCharts) {
        drawDomains(highlight);
        drawDependencies(highlight);
    }

}

function scaleAll(radius) {
    console.log('scaling: '+radius);
    for (var i  in countryBars) {
        if (!countryBars[i]) continue;
        var bars=countryBars[i];
        bars.setSize(0.005*Interactive.radius);
     }
    //bars.setSize(0.01*Interactive.radius);
    //globeObj.setScale(radius,radius,radius);
    countriesObj.setScale(radius,radius,radius);
}