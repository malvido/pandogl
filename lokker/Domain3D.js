// Domain3D.js :: a bar chart to display a domain on top of the world
// (c) 2020 Jose Alvarez

class Domain3D {
    constructor() {
        this.cube=null;
        this.highlightCube=null;

        this.baseMat=new Matrix4();

        this.x=0.0;
        this.y=0.0;
        this.z=0.0;

        this.rx=0.0;
        this.ry=0.0;
        this.rz=0.0;

        this.sizeX=10.0;
        this.sizeY=10.0;
        this.sizeZ=10.0;

        this.inner=1.0/1.41;

        this.size=1.0;

        this.dX=0;
        this.iX=0;

        this.iZ=0;

        this.hr=66.0/256.0;
        this.hg=0.0;
        this.hb=0.0;
        this.ha=1.0;

        this.setSize(this.size);

    }

    setCube(cube) {
        this.cube=cube;
    }

    setHighlightCube(cube) {
        this.highlightCube=cube;
    }

    setPosition(x,y,z) {
        this.x=x;
        this.y=y;
        this.z=z;
        
    }
        
    setRotate(x,y,z) {
        this.rx=x;
        this.ry=y;
        this.rz=z;
    }

    normalAlign() {
        var normal=new Vector3();
        normal.elements[1]=this.y;
        normal.elements[0]=this.x;
        normal.elements[2]=this.z;
        normal.normalize();
        this.setRotate(Math.acos(normal.elements[0])*180.0/Math.PI,
                Math.acos(normal.elements[1])*180.0/Math.PI,
                Math.acos(normal.elements[2])*180.0/Math.PI
            );
    }

    setSize(size) {
        this.sizeX*=(size/this.size);
        this.sizeY*=(size/this.size);
        this.sizeZ*=(size/this.size);
        this.size=size;

        // TODO
        this.dX=this.sizeX*this.inner/4;
        this.dZ=this.sizeZ*this.inner;
        
        this.iX=this.sizeX*(0.25-this.inner)/2.0;
        this.iZ=this.sizeZ*(0.75-this.inner)/2.0;
    }

    scale(size) {
        this.size*=size;
        this.dX*=size;
        this.iX*=size;
        this.iZ*=size;

    }

    setHighlightColor(r,g,b,a) {
        this.hr=r;
        this.hg=g;
        this.hb=b;
        this.ha=a;
    }

    setValues(infected,active,recovered,dead) {
        this.infected=infected;
        this.active=active;
        this.recovered=recovered;
        this.dead=dead;
    }

    calcCommon(mat) {
        mat.rotate(this.ry,0.0,1.0,0.0);
        mat.rotate(this.rx,1.0,0.0,0.0);
        mat.rotate(this.rz,0.0,0.0,1.0);
        mat.translate(this.x,this.y,this.z);
    }

    calcBase() {
        this.baseMat.setTranslate(0.0,0.0,0.0);
        this.calcCommon(this.baseMat);
        this.baseMat.translate(0.0,-this.size/2,0.0);
        this.baseMat.scale(this.sizeX,this.sizeY,this.sizeZ);
    }

    drawBase(r,g,b,a,highlight=false) {
        if (highlight) {
            this.highlightCube.setColor(this.hr/255.0,this.hg/255.0,this.hb/255.0,this.ha);
            this.calcBase();
            this.highlightCube.draw(this.baseMat);
            return;
        }
        this.cube.setColor(r,g,b,a);
        this.calcBase(r,g,b,a);
        this.cube.draw(this.baseMat);
    }

    draw(highlight=false) {
        if (highlight) {
            console.log(`${this.hr},${this.hg},${this.hb}`);
            this.drawBase(1,0.05,0.025,1.0,true);
        } else {
            this.drawBase(0.9,0.9,0.9,1.0);
        }
    }
}