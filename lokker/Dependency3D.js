// Dependency3D.js :: a class for drawing a dependency vector
// (c) 2021 Jose Alvarez

class Dependency3D {
    constructor(mercatorRows,mercatorCols,physTop=15000,glSL=0.85,glTop=0.05,numberOfVertices=15) {
        this.rows=mercatorRows;
        this.cols=mercatorCols;
        this.nOfVer=numberOfVertices;
        this.physTop=physTop;
        this.roSL=glSL;
        this.glTop=glTop;
        this.vertices=new Float32Array(this.nOfVer*4);
        this.colors=new Float32Array(this.nOfVer*4);

        this.transMatrix=new Matrix4();
    }

    setGLObj(glObject) {
        this.glObject=glObject;
    }

    latLonToXYZ(latitude,longitude) {
        let lat=(90.0-latitude);
        let Z=lat*Math.PI/180;
        let sinZ=Math.sin(Z);
        let cosZ=Math.cos(Z);

        let lon=(longitude-90);
        let P=lon*Math.PI/180;
        let sinP=Math.sin(P);
        let cosP=Math.cos(P);

        let retVal= {
            "x": sinZ*sinP
            ,"y": cosZ
            ,"z": sinZ*cosP
        }
        return retVal;
    }

    calcVertices(lat0,lon0,alt0,lat1,lon1,alt1) {
        //console.log("Coords: ",lat0,lon0,alt0,lat1,lon1,alt1);
        let ro=this.roSL+this.glTop/2;

        let ro0=ro+ro*alt0/20;
        let coords=this.latLonToXYZ(lat0,lon0);
        this.vertices[0]=ro0*coords.x;
        this.vertices[1]=ro0*coords.y;
        this.vertices[2]=ro0*coords.z;
        this.vertices[3]=1.0;

        let ro1=ro+ro*alt1/20;
        coords=this.latLonToXYZ(lat1,lon1);
        this.vertices[(this.nOfVer-1)*4]=ro1*coords.x;
        this.vertices[(this.nOfVer-1)*4+1]=ro1*coords.y;
        this.vertices[(this.nOfVer-1)*4+2]=ro1*coords.z;
        this.vertices[(this.nOfVer-1)*4+3]=1.0;

        let lastVert=this.nOfVer-1;
        for (let i=1;i<lastVert;i++) {
            let alt=(this.nOfVer-i)/this.nOfVer*alt0+i/this.nOfVer*alt1;
            let lat=(this.nOfVer-i)/this.nOfVer*lat0+i/this.nOfVer*lat1;
            let lon=(this.nOfVer-i)/this.nOfVer*lon0+i/this.nOfVer*lon1;
            let roN=ro+ro*alt/20;
            coords=this.latLonToXYZ(lat,lon);
            this.vertices[i*4]=roN*coords.x;
            this.vertices[i*4+1]=roN*coords.y;
            this.vertices[i*4+2]=roN*coords.z;
            this.vertices[i*4+3]=1.0;
        }
        //console.log(this.vertices);
    }

    calcColors() {
        for (let i=0;i<this.nOfVer;i++) {
            this.colors[i*4]=1.0-i/this.nOfVer;
            this.colors[i*4+1]=0.0;
            this.colors[i*4+2]=1.0*i/this.nOfVer;
            this.colors[i*4+3]=1.0
        }
    }

    setDependency(dep) {
        //console.log("Dep3D Parent: "+dep.parent.domain+" Child: "+dep.child.domain);
        this.calcVertices(
            dep.parent.lat,dep.parent.lon,dep.parent.altitude
            ,dep.child.lat,dep.child.lon,dep.child.altitude
        )
        this.calcColors();
    }

    create() {
        this.glObject.use();
        this.glObject.setNumberOfVertices(this.nOfVer);

        this.glObject.getAttributeLocation('a_Position');
        this.glObject.initArrayBufferFloat('a_Position',this.vertices,4);
        
        this.glObject.getAttributeLocation('a_Color');
        this.glObject.initArrayBufferFloat('a_Color',this.colors,4);
        
        this.glObject.setUniform1F('u_Size',1.0);

        this.glObject.getUniformLocation('u_Transform');
        this.glObject.world.unbindArrayBuffer();
    }

    draw(size) {
        this.glObject.use();
        this.glObject.setUniform1F('u_Size',size);
    
        this.glObject.drawLineStrip();
        this.glObject.world.unbindArrayBuffer();
    }
}