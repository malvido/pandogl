// Cube vertex shader
// (c) 2020 Jose Alvarez
attribute vec4 a_Position;
uniform vec4 u_Color;
uniform mat4 u_Transform;
uniform mat4 u_NormalMatrix;
varying vec4 v_Color;
void main() {
  gl_Position = u_Transform * a_Position;
  v_Color = u_Color;
}
