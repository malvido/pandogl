**Disclaimer**
It is sad to live in a world that needs this type of disclaimers, but it's the world we live in:

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

Straight out from the license to make it really clear.

**COVID Data**
The COVID-19 data comes straight from the [Johns Hopkins University repo](https://github.com/CSSEGISandData/COVID-19) and they collect it from a variety of sources that are not always reliable (governmental institutions, think about it). So, be very careful if you are going to make any important decisions based on the data.
The rights to the collection of the data belong to the Johns Hopkins University the formatting and grouping by country belong to me and any type of redistribution should follow the licensing terms.

**Geographical Data**
Similarly, all geographical data comes from [NASA](https://neo.sci.gsfc.nasa.gov/view.php?datasetId=SRTM_RAMP2_TOPO) which collects it from various sources probably much more reliable than the ones for COVID-19. Still, this particular set is way too coarse to be useful for any serious activity including but not limited to navigation and/or surveying. It may be okay for educational purposes, but not so sure about that either. Again the collection rights belong to NASA but the combination, transformation from Mercator into Globe format and other features belong to me, and any redistributions for whatever particular purpose should follow the licensing terms.

**Code**
Except for the external libraries in the extlib folder which belong to their proprietors (Kouichi Matsuda, Rodger Lea and Google) the rights for the rest of the code belong to me and any modification, redistribution and/or use should follow the licensing terms.

Once this is said, I must add that the code, right now, is mostly a mess as this was a WebGL learning project that got out of hand. Only a manager would put anything like this into production.